# Service Name : docitt.AssignmentEngine

- The purpose of assignment service is to invite user and assign role. 
- Used template-manager service to send invitation and message template.

## Getting Started

- Require environment setup.
- Repository and MongoDB access.
- Should have knowledge of Bit-bucket and DotNet CLI(Command-line interface ) commands. 
- Helpful links to start work :
	* https://docs.microsoft.com/en-us/dotnet/core/
	* https://docs.microsoft.com/en-us/dotnet/core/tools/?tabs=netcore2x
		
### Prerequisites

 - Windows, macOS or Linux Operating System.
 - .NET Core SDK (Software Development Kit).
 - .NET Core Command-line interface (CLI) or Visual Studio 2017.
 - MongoDB Management Tool.
 
### Installing

 - Clone source using below git command or source tree

	    git clone <repository_path>
	And go to respective branch for ex develop

		git fetch && git checkout develop	

 - Open command prompt and goto project directory.

 - Restore nuget packages by command

 		dotnet restore -s <Nuget Package Source> <solution_file>

		Ex. dotnet restore -s http://build.lendfoundry.co/guestAuth/app/nuget/v1/FeedService.svc docitt.AssignmentEngine.2017.sln 

 - Build solution by command : 

		dotnet build --no-restore <solution_file>

		Ex. dotnet build --no-restore docitt.AssignmentEngine.2017.sln
		
 - Run Solution by command
 
		dotnet run --no-build --project <project path>|
		eg. dotnet run --no-build --project src\Docitt.AssignmentEngine.Api\Docitt.AssignmentEngine.Api.csproj
		
## Running the tests

- We have created shellscript file(cover.sh) for running tests and code coverage, which are shared on lendfoundry documents.
- To start the test
	* open cmd 
	* goto project directory
	* type command cover.sh and enter
- you can see all tests results and code coverate report with covered number of lines and percentage on cmd.
- Once testing completed successfully from above command code coverage report html file is geneated on path :
 artifacts\coverage\report\coverage_report\index.html 

### What these tests test and why

- We are using xUnit testing and implementing for below projects.
	* Api
	* Client
	* Service
- xUnit.net is a free, open source, community-focused unit testing tool for the .NET Framework.
- This test includes all the scenarios which can be happen on realistic usage of this service.
- Help URL : https://docs.microsoft.com/en-us/dotnet/core/testing/unit-testing-with-dotnet-test

## Service Configuration

- Name :  assignment .
- Can get configuration by request below URL and token for specific tenant from postman
	* URL : <configuration_service>/assignment
	* Sample configurations :
	```
	{
    "affinityPartnerInvitationTemplate": {
        "name": <templatename>,
        "version": <version>
    },
    "lenderInvitationTemplate": {
        "name": <templatename>,
        "version": <version>
    },
    "borrowerInvitationTemplate": {
        "name": <templatename>,
        "version": <version>
    },
    "borrowerLoginPage": <borrowerLoginPageUrl>,
    "borrowerSignupPage": <borrowerSignupPageUrl>,
    "affinityPartnerLoginPage":<affinityPartnerLoginPageUrl>,
    "affinityPartnerSignupPage":<affinityPartnerSignupPageUrl>,
    "backOfficeLoginPage": <backOfficeLoginPageUrl>,
    "backOfficeSignupPage": <backOfficeSignupPageUrl>,
    "borrowerlendingpage": <borrowerlendingpageUrl>,
    "affinitypartnerlendingpage": <affinitypartnerlendingpageUrl>,
    "backOfficeLendingPage": <backOfficeLendingPageUrl>,
    "loanOfficerRole": <loanOfficerRole>
    "affinityPartnerAdminRole": <affinityPartnerAdminRole>,
    "borrowerroles": <Array(borrowerRoles)>,
    "tokentimeout":<tokentimeoutPeriods>,
    "sendInviteByEmail": <sendInviteByEmail>,
    "eventMappings":  <Array(eventMappings)>
	// ex: [
        {
            "EventName": "UserCreated",
            "Username": "{Data.Username}"
        },
        {
            "EventName": "UserProfileCreated",
            "Username": "{Data.UserProfile.Username}"
        }
    ]
}
	```


## Service Rules

NA

## Service API Documentation

- Anyone can access Api end points by entering below URL in any browser whenever service is running locally

		http://localhost:5000/swagger/

## Databases owned by email service

- Database : MongoDB
- Collections : assignment

## External Services called

- Sendgrid : Version= [9.9.0] 
	* This library is used for send emails functionality.	
- MimeKit : Version= [2.0.2] 
	* MimeKit is a C# library which may be used for the creation and parsing of messages using the Multipurpose Internet Mail Extension (MIME).
- MailKit : Version= [2.0.2] 
	* MailKit is a cross-platform mail client library built on top of MimeKit.

## Environment Variables used by email service

- Envirounment variables are application variables which are used for setting application behaviour at runtime. Ex. We can use different environment variables for different servers like DEV, QA etc.
- In lendfoundry any single service is dependent or uses many other lendfoundry services, So for dynamically target different services we are setting envirounment variable in launchSettings.json.
- Below are the envirounment variables in which we are setting only
important services URL's which are used in mostly all lendfoundry services and we are setting it in "src\LendFoundry.Email.Api\Properties\launchSettings.json".
- Other dependent service URL's are taken from configurations.

		"CONFIGURATION_NAME": "assignment",
		"CONFIGURATION_URL": <configuration service URL>,
		 // ex. "CONFIGURATION_URL": "http://10.100.0.12:5008",
		"EVENTHUB_URL": <event hub URL>,
		"NATS_URL": <nats server URL>,
		"TENANT_URL": <tenant server URL>,
		"LOG_LEVEL": "Debug"

## Known Issues and Workarounds

## Changelog