﻿using LendFoundry.Foundation.Client;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Docitt.AssignmentEngine.Client
{
    public class AssignmentService : IAssignmentService
    {
        public AssignmentService(IServiceClient client)
        {
            Client = client;
        }

        private IServiceClient Client { get; }


        public async Task<IAssignedResponse> IsAssigned(string entityType, string entityId)
        {
            return await Client.GetAsync<AssignedResponse>($"/{entityType}/{entityId}/assigned/me");
        }

        public async Task<IEnumerable<IAssignment>> Get(string entityType, string entityId)
        {
            return await Client.GetAsync<List<Assignment>>($"/{entityType}/{entityId}/assignments");
        }

        public async Task<IEnumerable<IAssignment>> GetByUser(string entityType, string username)
        {
            return await Client.GetAsync<List<Assignment>>($"/{entityType}/assignments/{username}");
        }

        public async Task<IAssignment> GetByRole(string entityType, string entityId,string role)
        { 
               return await Client.GetAsync<Assignment>($"{entityType}/{entityId}/assignment/{role}");
        }
        public async Task<IAssignment> Assign(string entityType, string entityId, IAssignmentRequest assignmentRequest)
        {
            return await Client.PostAsync<IAssignmentRequest,Assignment>($"/{entityType}/{entityId}/assign", assignmentRequest, true);
        }

        public async Task<IEnumerable<IAssignment>> MultiAssign(string entityType, string entityId, List<AssignmentRequest> assignmentRequest)
        {
            return await Client.PostAsync<List<AssignmentRequest>,List<Assignment>>($"/{entityType}/{entityId}/multiassign", assignmentRequest, true);
        }

        public async Task Unassign(string entityType, string entityId, string username)
        {
             await Client.DeleteAsync($"/{entityType}/{entityId}/disabled/{username}");
        }

        public async Task Release(string entityType, string entityId, string username)
        {
             await Client.PostAsync<dynamic>($"/{entityType}/{entityId}/release/{username}", null, true);
        }
    }
}