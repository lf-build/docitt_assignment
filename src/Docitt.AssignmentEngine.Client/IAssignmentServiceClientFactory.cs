﻿using LendFoundry.Security.Tokens;

namespace Docitt.AssignmentEngine.Client
{
    public interface IAssignmentServiceClientFactory
    {
        IAssignmentService Create(ITokenReader reader);
    }
}
