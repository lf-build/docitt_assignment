﻿using LendFoundry.Security.Tokens;
#if DOTNET2
using Microsoft.Extensions.DependencyInjection;
#else
using Microsoft.Framework.DependencyInjection;
#endif
using System;
namespace Docitt.AssignmentEngine.Client
{
    public static class AssignmentServiceClientExtensions
    {
        [Obsolete("Need to use the overloaded with Uri")]
        public static IServiceCollection AddAssignmentService(this IServiceCollection services, string endpoint, int port)
        {
            services.AddTransient<IAssignmentServiceClientFactory>(p => new AssignmentServiceServiceClientFactory(p, endpoint, port));
            services.AddTransient(p => p.GetService<IAssignmentServiceClientFactory>().Create(p.GetService<ITokenReader>()));

            return services;
        }

        public static IServiceCollection AddAssignmentService(this IServiceCollection services, Uri uri)
        {
            services.AddTransient<IAssignmentServiceClientFactory>(p => new AssignmentServiceServiceClientFactory(p, uri));
            services.AddTransient(p => p.GetService<IAssignmentServiceClientFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }

        public static IServiceCollection AddAssignmentService(this IServiceCollection services)
        {
            services.AddTransient<IAssignmentServiceClientFactory>(p => new AssignmentServiceServiceClientFactory(p));
            services.AddTransient(p => p.GetService<IAssignmentServiceClientFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }

        
    }
}
