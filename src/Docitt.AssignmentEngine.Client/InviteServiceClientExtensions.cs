﻿using LendFoundry.Security.Tokens;
#if DOTNET2
using Microsoft.Extensions.DependencyInjection;
#else
using Microsoft.Framework.DependencyInjection;
#endif
using System;
namespace Docitt.AssignmentEngine.Client
{
    public static class InviteServiceClientExtensions
    {
        [Obsolete("Need to use the overloaded with Uri")]
        public static IServiceCollection AddInviteService(this IServiceCollection services, string endpoint, int port)
        {
            services.AddTransient<IInviteServiceClientFactory>(p => new InviteServiceClientFactory(p, endpoint, port));
            services.AddTransient(p => p.GetService<IInviteServiceClientFactory>().Create(p.GetService<ITokenReader>()));

            return services;
        }

        public static IServiceCollection AddInviteService(this IServiceCollection services, Uri uri)
        {
            services.AddTransient<IInviteServiceClientFactory>(p => new InviteServiceClientFactory(p, uri));
            services.AddTransient(p => p.GetService<IInviteServiceClientFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }

        public static IServiceCollection AddInviteService(this IServiceCollection services)
        {
            services.AddTransient<IInviteServiceClientFactory>(p => new InviteServiceClientFactory(p));
            services.AddTransient(p => p.GetService<IInviteServiceClientFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }

      
    }
}
