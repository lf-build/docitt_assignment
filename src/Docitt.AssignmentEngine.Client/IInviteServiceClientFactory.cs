﻿using Docitt.AssignmentEngine.Services;
using LendFoundry.Security.Tokens;

namespace Docitt.AssignmentEngine.Client
{
    public interface IInviteServiceClientFactory
    {
        IInviteService Create(ITokenReader reader);
    }
}
