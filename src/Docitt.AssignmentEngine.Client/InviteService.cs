﻿using Docitt.AssignmentEngine.Services;
using System.Threading.Tasks;
using System;
using LendFoundry.Foundation.Client;
using System.Collections.Generic;
using Docitt.AssignmentEngine.Events;

namespace Docitt.AssignmentEngine.Client
{
    public class InviteService : IInviteService
    {
        private IServiceClient Client { get; }

        public InviteService(IServiceClient client)
        {
            Client = client;
        }

        public async Task<IInvite> Get(string inviteReferenceId)
        {
             return await Client.GetAsync<Invite>($"/invite/{inviteReferenceId}");
        }

        public async Task<IInviteVM> GetInviteData(string key, string value)
        {
             return await Client.GetAsync<InviteVM>($"key/{key}/value/{value}");
        }

        public async Task<IInvite> Get(string entityType, string invitereferenceid)
        {
            return await Client.GetAsync<Invite>($"{entityType}/invite/{invitereferenceid}");
        }


        public async Task<IInvite> VerifyToken(string entityType,IInviteVerifyRequest objRequest)
        {
            return await Client.PostAsync<IInviteVerifyRequest, Invite>($"/{entityType}/invite/verify", objRequest, true);
        }
        public async Task<IInvite> SendInvite(string entityType, string entityId, IInviteRequest inviteRequest, bool sendEmail=true)
        {
            string sendEmailString = sendEmail.ToString();
            return await Client.PostAsync<IInviteRequest, Invite>($"/{entityType}/{entityId}/invite/send/{sendEmailString}", inviteRequest, true);
        }

        public async Task<IInvite> UpdateInvite(string entityType, IInviteUpdateRequest updateRequest)
        {
            return await Client.PostAsync<IInviteUpdateRequest, Invite>($"/{entityType}/invite/update", updateRequest, true);
        }
        
        public Task<bool> UpdateInviteeNameDetails(string inviteId, string inviteFirstName, string inviteLastName)
        {
            throw new NotImplementedException();
        }

        public async Task<IAssignmentResponse> Assign(string entityType, string entityId, string userName)
        {
          return  await Client.PutAsync<dynamic, AssignmentResponse>($"/{entityType}/{entityId}/{userName}/assignment", null, true);
        }

        public async Task<string> GetAssociatedUserInfoByRole(string userName, string role)
        {
               var response= await Client.GetAsync<dynamic>($"/{userName}/{role}");
               return Convert.ToString(response);
        }

        public async Task<IInvite> GetInviteByUser(string userName)
        {
            return await Client.GetAsync<Invite>($"/invite/{userName}/info");
        }

        public async Task<IEnumerable<IInvite>> GetAllLeadInvite()
        {           
            return await Client.GetAsync<List<Invite>>($"/invite-lead/all");
        }

        public async Task<IEnumerable<IInvite>> GetAllLeadInvitedBy(string invitedByEmail)
        {
            return await Client.GetAsync<List<Invite>>($"/invite-lead/{invitedByEmail}");
        }

        public async Task<IInvite> GetLeadInviteInformationByInvitee(string inviteeEmail)
        {
             return await Client.GetAsync<Invite>($"/invite-lead/{inviteeEmail}/info");
        }

        public async Task<IInvite> SendReminder(string invitationId, bool sendEmail = true, string entityId = "")
        {
            var sendEmailString= Convert.ToString(sendEmail);
            return await Client.PutAsync<dynamic, Invite>($"/invite-lead/{invitationId}/send-reminder/{sendEmailString}/{entityId}", null);
        }
        
        public async Task<bool> ApplicationReminderNotification(string entityType, string entityId, string templateName, ApplicationReminderNotification payload)
        {
           var response = await Client.PostAsync<dynamic>($"/applicationremindernotification/{entityType}/{entityId}/{templateName}", payload, false);
           return Convert.ToBoolean(response);
        }

        public async Task<string> GetAssociatedUserInfoByRoleForLead(string invitationReferenceId, string role)
        {
            var response = await Client.GetAsync<dynamic>($"/invite-lead/{invitationReferenceId}/{role}");
            return Convert.ToString(response);
        }

        public async Task<bool> ExpireInvitation(string invitationId)
        {
            var response= await Client.PostAsync<dynamic, dynamic>($"/expireinvitation/{invitationId}", null, true);
            return Convert.ToBoolean(response);
        }
        
        
        public async Task<IEnumerable<IInvite>> GetInviteInfoByEmailAndRole(string entityType, string entityId, string invitedByEmail, string role)
        {
            return await Client.GetAsync<List<Invite>>($"{entityType}/{entityId}/invite-lead-info/{invitedByEmail}/{role}");
        }

        public async Task<bool> ExpireInvitations(IEnumerable<string> invitationIds)
        {
            var response =  await Client.PutAsync<dynamic,dynamic>($"/expire-invitations", invitationIds, true);
            return Convert.ToBoolean(response);
        }

        public async Task<IInvite> RegenerateInviteToken(string invitationId, string inviteEmailId)
        {
            var response =  await Client.PutAsync<IInvite, Invite>($"/regenerateinvitation/{invitationId}/{inviteEmailId}",null, true);
            return response;
        }
          public async Task<IInvite> GetInviteById(string invitationId)
        {
            return await Client.GetAsync<Invite>($"/invite-by-id/{invitationId}");
        }
     
    }
}
