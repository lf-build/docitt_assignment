﻿using System;
using System.Threading.Tasks;
using Docitt.AssignmentEngine.Services;
using LendFoundry.Foundation.Persistence.Mongo;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Serializers;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Linq;
using System.Collections.Generic;
using System.Linq;

namespace Docitt.AssignmentEngine.Persistence
{
    public class InviteRepository : MongoRepository<IInvite, Invite>, IInviteRepository
    {

        static InviteRepository()
        {
            BsonClassMap.RegisterClassMap<Invite>(map =>
            {
                map.AutoMap();
                map.MapMember(m => m.Team).SetSerializer(new EnumSerializer<Team>(BsonType.String));
                map.MapMember(m => m.InvitationDate).SetSerializer(new DateTimeOffsetSerializer(BsonType.Document));
                map.MapMember(m => m.InvitationAcceptedDate).SetSerializer(new DateTimeOffsetSerializer(BsonType.Document));

                var type = typeof(Invite);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(true);
            });
        }

        public InviteRepository(LendFoundry.Tenant.Client.ITenantService tenantService, IMongoConfiguration configuration)
            : base(tenantService, configuration, "invites")
        {
            CreateIndexIfNotExists("entity-type", Builders<IInvite>.IndexKeys.Ascending(i => i.TenantId).Ascending(i => i.EntityType));
            CreateIndexIfNotExists("entity-type-id", Builders<IInvite>.IndexKeys.Ascending(i => i.TenantId).Ascending(i => i.EntityType).Ascending(i => i.EntityId));

        }

        public async Task<IInvite> GetInviteById(string invitationId)
        {
            return await Query.FirstOrDefaultAsync(p => p.Id == invitationId);
        }

        public async Task<IInvite> GetInvite(string inviteReferenceId)
        {
            return await Query.FirstOrDefaultAsync(p => p.InvitationToken == inviteReferenceId);
        }

        public async Task<bool> UpdateInviteeNameDetails(string invitationId, string inviteFirstName, string inviteLastName)
        {
            var filter =  Builders<IInvite>.Filter
                .Where(a => a.TenantId == TenantService.Current.Id &&
                            a.Id == invitationId);
            
            var update = Builders<IInvite>.Update
                .Set(b => b.InviteFirstName, inviteFirstName)
                .Set(b => b.InviteLastName,inviteLastName);

            var result =  await Collection.UpdateOneAsync(filter, update);

            return result.IsModifiedCountAvailable;
        }

        /// <summary>
        /// GetInviteByUserName
        /// </summary>
        /// <param name="userName"></param>
        /// <returns></returns>
        public async Task<IInvite> GetInviteByUser(string userName)
        {
           return await Query.Where(p => p.UserName == userName.ToLower())
                                  .OrderByDescending(p => p.Id)
                                  .FirstOrDefaultAsync();
        }
        public async Task<IInvite> GetInvite(string entityType, string inviteRefId)
        {
            return await Query.FirstOrDefaultAsync(p => p.EntityType == entityType && p.InvitationToken == inviteRefId);
        }

        public async Task<IInvite> GetInvite(string entityType, string entityId, string inviteEmail)
        {
            return await Query.FirstOrDefaultAsync(p => p.EntityType == entityType && p.EntityId == entityId && p.InviteEmail == inviteEmail.ToLower());
        }

        public async Task<IInvite> GetInvite(string entityType, string entityId, IInviteRequest inviteRequest)
        {
            return await Query.Where(p => p.EntityType == entityType &&
                            p.EntityId == entityId &&
                            p.InvitedBy == inviteRequest.InvitedBy &&
                            p.InviteEmail.ToLower() == inviteRequest.InviteEmail.ToLower() &&
                            p.InviteFirstName == inviteRequest.InviteFirstName &&
                            p.InviteLastName == inviteRequest.InviteLastName &&
                            p.Role == inviteRequest.Role).OrderByDescending(x => x.InvitationDate).FirstOrDefaultAsync();
        }

        public async Task<IEnumerable<IInvite>> GetAllLeadInvite(string inviteLeadRole)
        {
            return await Query.Where(p => (p.UserName == null || p.UserName == "") && p.Role == inviteLeadRole).ToListAsync();
        }

        public async Task<IEnumerable<IInvite>> GetAllLeadInvitedBy(string invitedByEmail, string inviteLeadRole)
        {
            return await Query.Where(p => (p.UserName == null || p.UserName == "") && p.InvitedBy == invitedByEmail.ToLower() && p.Role == inviteLeadRole).ToListAsync();
        }

        public async Task<IInvite> GetLeadInviteInformationByInvitee(string inviteeEmail)
        {
            return await Query.FirstOrDefaultAsync(p => p.InviteEmail == inviteeEmail.ToLower());
        }

        public async Task<IInvite> GetInviteAndInvitedBy(string entityType, string entityId, string invitedByEmail, string inviteEmail)
        {
            return await Query.FirstOrDefaultAsync(p => p.EntityType == entityType && p.EntityId == entityId && p.InvitedBy == invitedByEmail.ToLower() && p.InviteEmail == inviteEmail.ToLower());
        }

        public async Task<IEnumerable<IInvite>> GetInviteInfoByEmailAndRole(string entityType, string entityId, string invitedByEmail, string role)
        {
            return await Query.Where(p => p.EntityType == entityType && p.EntityId == entityId && p.InvitedBy == invitedByEmail.ToLower() && p.Role == role).ToListAsync();
        }

        public async Task<bool> ExpireInvitations(IEnumerable<string> invitationToBeExpired)
        {

            var filter =  Builders<IInvite>.Filter
                    .Where(a => a.TenantId == TenantService.Current.Id &&
                                invitationToBeExpired.Contains(a.Id));

            
            var update = Builders<IInvite>.Update
                .Set(b => b.InvitationTokenExpiry, DateTime.UtcNow)
                .Set(b => b.UserId,null).Set(b => b.UserName, null);

           var result =  await Collection.UpdateManyAsync(filter,update);

            return result.IsModifiedCountAvailable;
        }

        public async Task<IInvite> RegenerateInviteToken(string invitationId, string inviteEmailId, string invitationToken, DateTime invitationTokenExpiration)
        {
            var invite = await Query.FirstOrDefaultAsync(p => p.Id == invitationId);

            if(invite == null || !string.IsNullOrEmpty(invite.UserId))
                throw new ArgumentException("Invitation is already used for signup or is not a valid id");

            var invitationUrl = invite.InvitationUrl.Replace(invite.InvitationToken, invitationToken);

            var filter =  Builders<IInvite>.Filter
                    .Where(a => a.TenantId == TenantService.Current.Id &&
                                a.Id == invitationId);

            var update = Builders<IInvite>.Update
                .Set(b => b.InvitationTokenExpiry, invitationTokenExpiration)
                .Set(b => b.InvitationToken, invitationToken)
                .Set(b => b.InviteEmail, inviteEmailId.ToLower())
                .Set(b => b.InvitationUrl, invitationUrl);

            var result =  await Collection.UpdateManyAsync(filter,update);

            return await Query.FirstOrDefaultAsync(p => p.Id == invitationId);
        }
    }
}
