﻿using Docitt.AssignmentEngine;
using LendFoundry.Foundation.Persistence.Mongo;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Serializers;
using MongoDB.Driver;
using MongoDB.Driver.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Docitt.AssignmentEngine.Persistence
{
    public class AssignmentRepository : MongoRepository<IAssignment, Assignment>, IAssignmentRepository
    {
        static AssignmentRepository()
        {
            BsonClassMap.RegisterClassMap<Assignment>(map =>
            {
                map.AutoMap();
                map.MapMember(m => m.Team).SetSerializer(new EnumSerializer<Team>(BsonType.String));
                map.MapMember(m => m.AssignedOn).SetSerializer(new DateTimeOffsetSerializer(BsonType.Document));
                var type = typeof(Assignment);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(true);
            });
        }

        public AssignmentRepository(LendFoundry.Tenant.Client.ITenantService tenantService, IMongoConfiguration configuration)
            : base(tenantService, configuration, "assignments")
        {
            CreateIndexIfNotExists("entity-type", Builders<IAssignment>.IndexKeys.Ascending(i => i.TenantId).Ascending(i => i.EntityType));
            CreateIndexIfNotExists("entity-type-id", Builders<IAssignment>.IndexKeys.Ascending(i => i.TenantId).Ascending(i => i.EntityType).Ascending(i => i.EntityId));
            CreateIndexIfNotExists("user-name", Builders<IAssignment>.IndexKeys.Ascending(i => i.TenantId).Ascending(i => i.Assignee));
            CreateIndexIfNotExists("role", Builders<IAssignment>.IndexKeys.Ascending(i => i.TenantId).Ascending(i => i.Role));
            CreateIndexIfNotExists("assignee", Builders<IAssignment>.IndexKeys.Ascending(i => i.TenantId).Ascending(i => i.EntityType).Ascending(i => i.EntityId).Ascending(i=>i.Assignee),true);
            //CreateIndexIfNotExists("escalation-id", Builders<IAssignment>.IndexKeys.Ascending(i => i.TenantId).Ascending(i => i.EscalationId));
        }

        public async Task<IAssignment> GetByUserName(string entityType, string entityId, string username)
        {
            return
                await
                    Query.FirstOrDefaultAsync(
                        p => p.EntityType == entityType && p.EntityId == entityId && p.Assignee == username.ToLower() && p.IsActive && !p.IsDisable);
        }

        public async Task<IEnumerable<IAssignment>> Get(string entityType, string entityId)
        {
            return await Query.Where(p => p.EntityType == entityType && p.EntityId == entityId && p.IsActive && !p.IsDisable).ToListAsync();
        }

        public async Task<IEnumerable<IAssignment>> GetByUserName(string entityType, string username)
        {
            return await Query.Where(p => p.EntityType == entityType && p.Assignee == username.ToLower() && p.IsActive && !p.IsDisable).ToListAsync();
        }

        public async Task<IAssignment> GetByRole(string entityType, string entityId, string role )
        {
            return
                await Query.FirstOrDefaultAsync(p => p.EntityType == entityType && p.EntityId == entityId && p.Role.ToLower() == role.ToLower() && p.IsActive && !p.IsDisable);
        }

        //public async Task<IAssignment> GetByEscalationId(string entityType, string entityId, string escalationId)
        //{
        //    return await Query.FirstOrDefaultAsync(p => p.EntityType == entityType && p.EntityId == entityId && p.EscalationId == escalationId);
        //}

    }
}