﻿namespace Docitt.AssignmentEngine
{
    public interface IEnsureHelper
    {
        void EnsureCreateInvite(IInviteRequest inviteRequest);
        void EnsureUpdateInvite(IInviteUpdateRequest inviteRequest);
    }
}
