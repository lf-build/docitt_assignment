﻿using System;
using System.Collections.Generic;
using LendFoundry.Foundation.Persistence;

namespace Docitt.AssignmentEngine
{
    public interface IInvite : IAggregate
    {
        string EntityId { get; set; }
        string EntityType { get; set; }
        DateTimeOffset InvitationDate { get; set; }
        string InvitedBy { get; set; }
        string InviteEmail { get; set; }
        string InviteMobileNumber { get; set; }
        string InviteFirstName { get; set; }
        string InviteLastName { get; set; } 
        string Role { get; set; }
        string UserName { get; set; }
        string UserId { get; set; }
        string InvitationUrl { get; set; }
        DateTimeOffset InvitationAcceptedDate { get; set; }
        string InvitationToken { get; set; }
        DateTime? InvitationTokenExpiry { get; set; }
        Team Team { get; set; }

        DateTimeOffset? LastReminder { get; set; }

        List<string> ReferenceApplicationNumbers { get; set; }
    }
}