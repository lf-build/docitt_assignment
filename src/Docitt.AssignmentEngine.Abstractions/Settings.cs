
using System;

namespace Docitt.AssignmentEngine
{
    public static class Settings
    {      

        public static string ServiceName => Environment.GetEnvironmentVariable($"CONFIGURATION_NAME") ?? "assignment";

    }
}