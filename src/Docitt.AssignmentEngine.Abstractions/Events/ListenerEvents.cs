﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Docitt.AssignmentEngine
{
   public enum ListenerEvents
    {
        SpouseCopiedSuccessFully,
        UpdateSpouseInvitationDetails
    }
}
