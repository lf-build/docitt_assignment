﻿namespace Docitt.AssignmentEngine.Events
{
    public class AssignmentAssigned
    {
        public AssignmentAssigned(IAssignment assignment)
        {
            Assignment = assignment;
        }
       public IAssignment Assignment { get; set; }
    }
}
