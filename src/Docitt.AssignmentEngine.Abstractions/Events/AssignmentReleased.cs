﻿namespace Docitt.AssignmentEngine.Events
{
    public class AssignmentReleased
    {
        public AssignmentReleased(IAssignment assignment)
        {
            Assignment = assignment;
        }
       public IAssignment Assignment { get; set; }
    }
}
