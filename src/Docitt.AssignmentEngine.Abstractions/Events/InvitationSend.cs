﻿

namespace Docitt.AssignmentEngine.Events
{
    public class InvitationSend
    {
        public InvitationSend(IInvite inviteData)
        {           
            InviteData = inviteData;
        }
        public IInvite InviteData { get; set; }
    }
}
