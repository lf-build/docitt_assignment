﻿namespace Docitt.AssignmentEngine.Events
{
    public class AssignmentUnassigned
    {
        public AssignmentUnassigned(IAssignment assignment)
        {
            Assignment = assignment;
        }
       public IAssignment Assignment { get; set; }
    }
}
