﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Docitt.AssignmentEngine.Events
{
    public class InvitationReminderSend
    {
        public InvitationReminderSend(IInvite inviteData)
        {
            InviteData = inviteData;
        }
        public IInvite InviteData { get; set; }
    }
}