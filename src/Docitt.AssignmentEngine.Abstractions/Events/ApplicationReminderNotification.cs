using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Threading.Tasks;

namespace Docitt.AssignmentEngine.Events
{
    public class ApplicationReminderNotification
    {
        public string EntityId {get; set;}
        public string BorrowerFirstName { get; set; }
        public string BorrowerLastName { get; set; }
        public string Email { get; set; }
        public string BorrowerPortalUrl { get; set; }
        public string LoanOfficerName { get; set; }
        public string LoanOfficerNMLS { get; set; }
        public string LoanOfficerPhoneNumber {get; set;}
        public string LoanOfficerEmail {get; set;}
        public string InitiatedByName {get; set;}
        public string InitiatedByPhoneNumber {get; set;}
        public string InitiatedByEmail {get; set;}
    }
}