namespace Docitt.AssignmentEngine.Events
{
    public class RegenerateInvitation
    {
        public RegenerateInvitation(IInvite inviteData)
        {
            InviteData = inviteData;
        }
        public IInvite InviteData { get; set; }
    }
}