﻿
namespace Docitt.AssignmentEngine.Events
{
    public class InvitationAccepted
    {
        public InvitationAccepted(IInvite inviteData)
        {
            InviteData = inviteData;
        }
        public IInvite InviteData { get; set; }
    }
}
