﻿using System;
using LendFoundry.Foundation.Persistence;

namespace Docitt.AssignmentEngine
{
    public interface IAssignment : IAggregate
    {
        string EntityType { get; set; }
        string EntityId { get; set; }
        string Role { get; set; }
        string UserId { get; set; }
        string Assignee { get; set; }
        DateTimeOffset AssignedOn { get; set; }
        string AssignedBy { get; set; }   
        bool IsActive { get; set; }
        bool IsDisable { get; set; }
        Team Team { get; set; }
    }
}
