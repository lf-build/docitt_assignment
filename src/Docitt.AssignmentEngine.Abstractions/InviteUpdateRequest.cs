﻿
namespace Docitt.AssignmentEngine
{
    public class InviteUpdateRequest : IInviteUpdateRequest
    {
        public string UserName { get; set; }

        public string UserId { get; set; }

        public string InviteRefId { get; set; }

        public string EmailAddress {get; set;}
    }
}
