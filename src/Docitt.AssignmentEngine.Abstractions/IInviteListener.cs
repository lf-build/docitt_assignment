﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Docitt.AssignmentEngine
{
    public interface IInviteListener
    {
        void Start();
    }
}
