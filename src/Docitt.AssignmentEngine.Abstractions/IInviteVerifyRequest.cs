﻿
namespace Docitt.AssignmentEngine
{
    public interface IInviteVerifyRequest
    {
        string InviteRefId { get; set; }
        string InviteEmail { get; set; }
        string Username {get; set;}
    }
}
