﻿using System;
using LendFoundry.Foundation.Persistence;

namespace Docitt.AssignmentEngine
{
    public class Assignment : Aggregate, IAssignment
    {
        public string EntityId { get; set; }
        public string EntityType { get; set; }

        public string UserId { get; set; }
        public string Assignee { get; set; }
        public string Role { get; set; }
        public string AssignedBy { get; set; }
        public DateTimeOffset AssignedOn { get; set; }
        public string EscalationId { get; set; }
        public bool IsActive { get; set; }
        public bool IsDisable { get; set; }
        public Team Team { get; set; }
    }
}
