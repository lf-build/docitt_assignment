﻿using Docitt.AssignmentEngine.Services;
using LendFoundry.Foundation.Logging;
using LendFoundry.Security.Tokens;

namespace Docitt.AssignmentEngine
{
    public interface IValidatorServiceFactory
    {
        IValidatorService Create(ITokenReader reader, ILogger logger);
    }
}