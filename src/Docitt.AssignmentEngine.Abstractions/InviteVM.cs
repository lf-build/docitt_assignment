﻿using LendFoundry.Foundation.Persistence;
using System;
using System.Collections.Generic;

namespace Docitt.AssignmentEngine
{
    public class InviteVM : IInviteVM
    {
        public List<InvitedBy> InvitedBy { get; set; }
        public string InviteEmail { get; set; }
        public string InviteUserName {get; set;}
        public string InviteMobileNumber { get; set; }
        public string InviteFirstName { get; set; }
        public string InviteLastName { get; set; }
        public string Role { get; set; }
        public Team Team { get; set; }
    }

    public class InvitedBy
    {
        public string Username {get; set;}
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MobileNumber { get; set; }

         public string PhoneNumber { get; set; }
        public string Role { get; set; }
        public string NmlsNumber { get; set; }
        public bool isLoanOfficer { get; set; }
    }
}
