﻿namespace Docitt.AssignmentEngine
{
    public interface IAssignmentRequest
    {
        string UserId { get; set; }
        string User { get; set; }
        string Role { get; set; }
        string Team { get; set; }
    }
}