﻿namespace Docitt.AssignmentEngine
{
    public class AssignedResponse : IAssignedResponse
    {

        public AssignedResponse()
        {
            
        }

        public AssignedResponse(bool isAssigned)
        {
            IsAssigned = isAssigned;
        }


        public bool IsAssigned { get; set; }
    }
}
