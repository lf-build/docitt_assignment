﻿using LendFoundry.Foundation.Persistence;
using System;
using System.Collections.Generic;

namespace Docitt.AssignmentEngine
{
    public class Invite : Aggregate, IInvite
    {
        public string EntityType { get; set; }
        public string EntityId { get; set; }       
        public string InvitedBy { get; set; }
        public string InviteEmail { get; set; }
        public string InviteFirstName { get; set; }
        public string InviteLastName { get; set; }
        public string InviteMobileNumber { get; set; }
        public DateTimeOffset InvitationDate { get; set; }
        public string Role { get; set; }

        public string UserName { get; set; }

        public string UserId { get; set; }
        public string InvitationUrl { get; set; }
        public DateTimeOffset InvitationAcceptedDate { get; set; }

        public string InvitationToken { get; set; }
        public DateTime? InvitationTokenExpiry { get; set; }
        public Team Team { get; set; }

        public DateTimeOffset? LastReminder { get; set; }

        public List<string> ReferenceApplicationNumbers { get; set; }
    }
}
