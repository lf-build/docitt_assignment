﻿
namespace Docitt.AssignmentEngine
{
    public class InviteVerifyRequest : IInviteVerifyRequest
    {
        public string InviteRefId { get; set; }
        public string InviteEmail { get; set; }
    
        public string Username {get; set;}
    }
}
