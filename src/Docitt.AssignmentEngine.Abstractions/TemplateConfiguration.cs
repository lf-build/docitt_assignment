namespace Docitt.AssignmentEngine
{
    public class TemplateConfiguration
    {
        public string Name { get; set; }
        public string Version { get; set; }
    }
}