﻿using System;
using LendFoundry.Foundation.Persistence;

namespace Docitt.AssignmentEngine
{
    public interface IAssignmentResponse
    {
        string EntityType { get; set; }
        string EntityId { get; set; }
        string Role { get; set; }
        string Assignee { get; set; }
        DateTimeOffset AssignedOn { get; set; }
        string AssignedBy { get; set; }
        //IEscalation Escalation { get; set; }
    }
}
