﻿namespace Docitt.AssignmentEngine
{
    public interface IInviteUpdateRequest
    {
        string InviteRefId { get; set; }
        string UserId { get; set; }
        string UserName { get; set; }
        string EmailAddress {get; set;}
    }
}