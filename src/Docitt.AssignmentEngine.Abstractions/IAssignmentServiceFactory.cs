﻿using LendFoundry.Foundation.Logging;
using LendFoundry.Security.Tokens;

namespace Docitt.AssignmentEngine
{
    public interface IAssignmentServiceFactory
    {
        IAssignmentService Create(ITokenReader reader, ILogger logger);
    }
}