﻿using System;
using LendFoundry.Foundation.Persistence;
using System.Collections.Generic;

namespace Docitt.AssignmentEngine
{
    public interface IInviteVM
    {
        List<InvitedBy> InvitedBy { get; set; }
        string InviteEmail { get; set; }
        string InviteUserName {get; set;}
        string InviteMobileNumber { get; set; }
        string InviteFirstName { get; set; }
        string InviteLastName { get; set; }
        string Role { get; set; }
        Team Team { get; set; }
    }
}