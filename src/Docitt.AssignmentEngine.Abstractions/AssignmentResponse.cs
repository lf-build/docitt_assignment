﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Docitt.AssignmentEngine
{
    public class AssignmentResponse : IAssignmentResponse
    {
        public string EntityType { get; set; }
        public string EntityId { get; set; }
        public string Role { get; set; }
        public string Assignee { get; set; }
        public DateTimeOffset AssignedOn { get; set; }
        public string AssignedBy { get; set; }
    }
}
