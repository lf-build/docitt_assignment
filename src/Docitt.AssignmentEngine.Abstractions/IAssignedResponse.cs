﻿namespace Docitt.AssignmentEngine
{
    public interface IAssignedResponse
    {
        bool IsAssigned { get; set; }
    }
}
