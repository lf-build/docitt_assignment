﻿
namespace Docitt.AssignmentEngine
{
    public class InviteRequest : IInviteRequest
    {
        public string InvitedBy { get; set; }
        public string InviteEmail { get; set; }
        public string InviteFirstName { get; set; }
        public string InviteLastName { get; set; }
        public string InviteMobileNumber { get; set; }
        public string Role { get; set; }
        public string Team { get; set; }

        public string CountryCode {get;set;}

        public bool SendBothEmailText{get;set;}

        public bool isLenderNewLoan {get;set;}
    }
}
