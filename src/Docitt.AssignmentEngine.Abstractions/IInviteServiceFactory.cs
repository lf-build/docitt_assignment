﻿using Docitt.AssignmentEngine.Services;
using LendFoundry.Security.Tokens;
using LendFoundry.Foundation.Logging;

namespace Docitt.AssignmentEngine
{
    public interface IInviteServiceFactory
    {
        IInviteService Create(ITokenReader reader, ILogger logger, ITokenHandler tokenHandler);
    }
}
