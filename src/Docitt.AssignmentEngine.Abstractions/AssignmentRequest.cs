﻿namespace Docitt.AssignmentEngine
{
    public class AssignmentRequest : IAssignmentRequest
    {
        public string UserId { get; set; }
        public string User { get; set; }
        public string Role { get; set; }
        public string Team { get; set; }
    }
}