﻿
namespace Docitt.AssignmentEngine
{
    public interface IInviteRequest
    {
      
        string InvitedBy { get; set; }
        string InviteEmail { get; set; }
        string InviteFirstName { get; set; }
        string InviteLastName { get; set; }
        string InviteMobileNumber { get; set; }
        string Role { get; set; }
        string Team { get; set; }
        string CountryCode {get;set;}

        bool SendBothEmailText{get;set;}

        bool isLenderNewLoan {get;set;}
    }
}
