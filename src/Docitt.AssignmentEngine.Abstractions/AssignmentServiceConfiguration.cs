using LendFoundry.Foundation.Client;
using System.Collections.Generic;

namespace Docitt.AssignmentEngine
{
    
    public class AssignmentServiceConfiguration : IDependencyConfiguration
    {
         public TemplateConfiguration AffinityPartnerTextMessageTemplate { get; set; }
        public TemplateConfiguration BorrowerTextMessageTemplate { get; set; }
        public TemplateConfiguration AffinityPartnerInvitationTemplate { get; set; }
        public TemplateConfiguration LenderInvitationTemplate { get; set; }
        public TemplateConfiguration BorrowerInvitationTemplate { get; set; }
        public string BorrowerLendingPage { get; set; }
        public string AffinityPartnerLendingPage { get; set; }
        public string BackOfficeLendingPage { get; set; }

        public string BorrowerLoginPage { get; set; }
        public string BorrowerSignupPage { get; set; }
        public string AffinityPartnerLoginPage { get; set; }
        public string AffinityPartnerSignupPage { get; set; }
        public string BackOfficeLoginPage { get; set; }
        public string BackOfficeSignupPage { get; set; }

        public string LoanOfficerRole { get; set; }
        public string[] BorrowerRoles { get; set; }
        public int TokenTimeout { get; set; }
        public List<EventMapping> EventMappings { get; set; }

        public string AffinityPartnerAdminRole { get; set; }
        public Dictionary<string, string> Dependencies { get; set; }
        public string Database { get; set; }

        public string InviteLeadRole { get; set; }
    }
}