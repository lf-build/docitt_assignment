﻿using LendFoundry.Security.Tokens;


namespace Docitt.AssignmentEngine.Services
{
    public interface IInviteRepositoryFactory
    {
        IInviteRepository Create(ITokenReader reader);
    }
}
