﻿using LendFoundry.Security.Tokens;

namespace Docitt.AssignmentEngine
{
    public interface IAssignmentRepositoryFactory
    {
        IAssignmentRepository Create(ITokenReader reader);
    }
}
