﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Docitt.AssignmentEngine.Events;

namespace Docitt.AssignmentEngine.Services
{
   public interface IInviteService
    {
        Task<IAssignmentResponse> Assign(string entityType, string entityId, string userName);
        Task<IInvite> Get(string inviteReferenceId);
        Task<IInviteVM> GetInviteData(string key, string value);
        Task<IInvite> SendInvite(string entityType, string entityId, IInviteRequest inviteRequest, bool sendEmail = true);
        Task<IInvite> VerifyToken(string entityType, IInviteVerifyRequest inviteVerifyRequest);
        Task<IInvite> UpdateInvite(string entityType, IInviteUpdateRequest updateRequest);
        Task<IInvite> GetInviteByUser(string userName);
        Task<string> GetAssociatedUserInfoByRole(string userName, string role);
        Task<IEnumerable<IInvite>> GetAllLeadInvite();
        Task<IEnumerable<IInvite>> GetAllLeadInvitedBy(string invitedByEmail);
        Task<IInvite> GetLeadInviteInformationByInvitee(string inviteeEmail);
        Task<IInvite> SendReminder(string invitationId, bool sendEmail = true, string entityId = "");
        Task<bool> ApplicationReminderNotification(string entityType, string entityId, string templateName, ApplicationReminderNotification payload);
        Task<string> GetAssociatedUserInfoByRoleForLead(string invitationReferenceId, string role);

        Task<bool> ExpireInvitation(string invitationId);
        Task<IEnumerable<IInvite>> GetInviteInfoByEmailAndRole(string entityType, string entityId, string invitedByEmail, string role);
        Task<bool> ExpireInvitations(IEnumerable<string> invitationIds);
        Task<bool> UpdateInviteeNameDetails(string inviteId, string inviteFirstName, string inviteLastName);
        Task<IInvite> RegenerateInviteToken(string invitationId, string inviteEmailId);
        Task<IInvite> GetInviteById(string invitationId);
        
    }
}
