﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Docitt.AssignmentEngine
{
    public interface IAssignmentService
    {
        Task<IAssignedResponse> IsAssigned(string entityType, string entityId);
        Task<IEnumerable<IAssignment>> Get(string entityType, string entityId);
        Task<IEnumerable<IAssignment>> GetByUser(string entityType, string username);
        Task<IAssignment> Assign(string entityType, string entityId, IAssignmentRequest assignmentRequest);
        Task<IEnumerable<IAssignment>> MultiAssign(string entityType, string entityId, List<AssignmentRequest> assignmentRequest);
        Task Unassign(string entityType, string entityId, string role);
        Task Release(string entityType, string entityId, string role = null);
        Task<IAssignment> GetByRole(string entityType, string entityId, string role);
    }
}