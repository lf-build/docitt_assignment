﻿using System.Collections.Generic;
using System.Threading.Tasks;
using LendFoundry.Foundation.Persistence;

namespace Docitt.AssignmentEngine
{
    public interface IAssignmentRepository : IRepository<IAssignment>
    {
        Task<IAssignment> GetByUserName(string entityType, string entityId, string username);
        Task<IEnumerable<IAssignment>> Get(string entityType, string entityId);
        Task<IEnumerable<IAssignment>> GetByUserName(string entityType, string username);
        Task<IAssignment> GetByRole(string entityType, string entityId, string role);
        //Task<IAssignment> GetByEscalationId(string entityType, string entityId, string escalationId);
    }
}