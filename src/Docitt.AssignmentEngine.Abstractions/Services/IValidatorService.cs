﻿using System.Threading.Tasks;

namespace Docitt.AssignmentEngine.Services
{
    public interface IValidatorService
    {
        string EnsureEntityType(string entityType);
        void EnsureEntityId(string entityId);
        string EnsureCurrentUser();
        string EnsureFirstCharacterCapital(string value);
        string EnsureEmailAddress(string value);
    }
}
