﻿using LendFoundry.Foundation.Persistence;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Docitt.AssignmentEngine.Services
{
    public interface IInviteRepository : IRepository<IInvite>
    {
        Task<IInvite> GetInvite(string inviteReferenceId);
        Task<IInvite> GetInvite(string entityType, string inviteRefId);

        Task<IInvite> GetInviteByUser(string userName);
        Task<IInvite> GetInvite(string entityType, string entityId, string inviteEmail);
        Task<IInvite> GetInvite(string entityType, string entityId, IInviteRequest inviteRequest);

        Task<IEnumerable<IInvite>> GetAllLeadInvite(string inviteLeadRole);
        Task<IEnumerable<IInvite>> GetAllLeadInvitedBy(string invitedByEmail, string inviteLeadRole);
        Task<IInvite> GetLeadInviteInformationByInvitee(string inviteeEmail);
        Task<IInvite> GetInviteById(string invitationId);
        Task<IInvite> GetInviteAndInvitedBy(string entityType, string entityId, string invitedByEmail, string inviteEmail);
        Task<IEnumerable<IInvite>> GetInviteInfoByEmailAndRole(string entityType, string entityId, string invitedByEmail, string role);
        Task<bool> ExpireInvitations(IEnumerable<string> invitationToBeExpired);
        Task<bool> UpdateInviteeNameDetails(string invitationId, string inviteFirstName, string inviteLastName);
        Task<IInvite> RegenerateInviteToken(string invitationId, string inviteEmailId, string invitationToken, DateTime invitationTokenExpiry);
    }
}
