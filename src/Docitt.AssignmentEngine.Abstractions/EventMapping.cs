﻿namespace Docitt.AssignmentEngine
{
    public class EventMapping
    {
        public string EventName { get; set; }
        public string Username { get; set; } 
        public string EntityId { get; set; }
        
        public string InviteId { get; set; }
        
        public string FirstName { get; set; }
        
        public string LastName { get; set; }
    }
}
