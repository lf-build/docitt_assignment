﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Docitt.AssignmentEngine.Events;
using Docitt.AssignmentEngine.Services;
using Docitt.TwilioOTP;
using Docitt.UserProfile;
using LendFoundry.Configuration;
using LendFoundry.Email;
using LendFoundry.EventHub;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Security.Identity.Client;
using LendFoundry.Security.Tokens;
using LendFoundry.TemplateManager;

namespace Docitt.AssignmentEngine
{
    public class InviteService : IInviteService
    {
        public InviteService(
            IAssignmentService assignmentService,
            IInviteRepository repository,
            ITenantTime tenantTime,
            IEventHubClient eventHub,
            IValidatorService validator,
            IEmailService emailService,
            AssignmentServiceConfiguration configuration,
            IIdentityService identityService,
            IUserProfileService userProfile,
            IEnsureHelper ensureHelper,
            ILogger logger,
            ITextMessageService textMessageService,
            ITemplateManagerService templateManager,
            IConfigurationServiceFactory configurationServiceFactory,
            ITokenHandler tokenHandler,
            ITokenReader tokenReader)
        {
            if (assignmentService == null)
                throw new ArgumentException($"{nameof(assignmentService)} is mandatory");
            if (repository == null)
                throw new ArgumentException($"{nameof(repository)} is mandatory");
            if (eventHub == null)
                throw new ArgumentException($"{nameof(eventHub)} is mandatory");
            if (emailService == null)
                throw new ArgumentException($"{nameof(emailService)} is mandatory");
            if (tenantTime == null)
                throw new ArgumentException($"{nameof(tenantTime)} is mandatory");
            if (validator == null)
                throw new ArgumentException($"{nameof(validator)} is mandatory");
            if (configuration == null)
                throw new ArgumentException($"{nameof(configuration)} is mandatory");
            if (identityService == null)
                throw new ArgumentException($"{nameof(identityService)} is mandatory");
            if (userProfile == null)
                throw new ArgumentException($"{nameof(userProfile)} is mandatory");
            if (textMessageService == null)
                throw new ArgumentException($"{nameof(textMessageService)} is mandatory");
            if (templateManager == null)
                throw new ArgumentException($"{nameof(templateManager)} is mandatory");

            Repository = repository;
            TenantTime = tenantTime;
            EventHub = eventHub;
            EmailService = emailService;
            Validator = validator;
            Configuration = configuration;
            AssignmentService = assignmentService;
            IdentityService = identityService;
            UserProfile = userProfile;
            EnsureHelper = ensureHelper;
            Log = logger;
            MessageService = textMessageService;
            TemplateManager = templateManager;
            TokenHandler = tokenHandler;
            TokenReader = tokenReader;
            ConfigurationServiceFactory = configurationServiceFactory;
        }

        private ITemplateManagerService TemplateManager { get; }
        private ILogger Log { get; }

        private IIdentityService IdentityService { get; }

        private IUserProfileService UserProfile { get; }
        IAssignmentService AssignmentService { get; }
        private IInviteRepository Repository { get; }
        private IEmailService EmailService { get; }
        private ITenantTime TenantTime { get; }
        private IEventHubClient EventHub { get; }
        private IValidatorService Validator { get; }
        private IEnsureHelper EnsureHelper { get; }
        private AssignmentServiceConfiguration Configuration { get; }

        private ITextMessageService MessageService { get; }
        private ITokenHandler TokenHandler { get; }
        private ITokenReader TokenReader { get; }
        private IConfigurationServiceFactory ConfigurationServiceFactory { get; }

        public async Task<IAssignmentResponse> Assign(string entityType, string entityId, string userName)
        {
            Log.Debug($"Started Assign...");

            Log.Debug($"... validate entityType {entityType}");
            Validator.EnsureEntityType(entityType);

            Log.Debug($"... validate entityId {entityId}"); // temporary application number
            Validator.EnsureEntityId(entityId);

            if (string.IsNullOrEmpty(Configuration.LoanOfficerRole))
                throw new InvalidOperationException("configuration for loan office role is not found.");

            Log.Debug($"... create applicationTeam");
            List<AssignmentRequest> applicationTeam = new List<AssignmentRequest>();
            var assignmentTasks = new List<Task>();

            //first we need to get the invitation reference id & role from userName
            // IInvite objLoanOfficerInvite = null;
            var borroweruser = await GetUser(userName);
            IAssignmentResponse assignmentResponse = null;

            if (borroweruser != null && (
                    (!string.IsNullOrEmpty(borroweruser.InvitationReferenceId)) ||
                    (!string.IsNullOrEmpty(borroweruser.BrandedSiteReferenceId))
            ))
            {
                Log.Debug($"... starting workflow for Invitation");

                //need to add borrower into team :
                applicationTeam.Add(new AssignmentRequest()
                {
                    User = borroweruser.Username.ToLower(),
                    Role = borroweruser.Roles[0],
                    Team = Team.contributor.ToString(),
                    UserId = borroweruser.Id
                });
                Log.Debug($"... association of borrower completed");

                string loanOfficerUserName = string.Empty;
                string affinityPartnerAdminUserName = string.Empty;

                Func<Task> loanOfficerTask = async () =>
                {
                    Log.Debug($"... association of loan officer started");
                    //first we have to get the loan office associated with this user using invitation and add his team
                    try
                    {
                        Log.Debug($"... associate loan officer");
                        loanOfficerUserName = await GetAssociatedUserInfoByRole(userName, Configuration.LoanOfficerRole);
                        await AssignTeam(applicationTeam, loanOfficerUserName, Team.lender.ToString());

                        if (!string.IsNullOrEmpty(loanOfficerUserName))
                        {
                            Log.Debug($"... create assignment response");
                            assignmentResponse = new AssignmentResponse()
                            {
                                EntityType = entityType,
                                EntityId = entityId,
                                Role = Configuration.LoanOfficerRole,
                                Assignee = loanOfficerUserName
                            };
                        }
                    }
                    catch (Exception ex)
                    {
                        Log.Info($"... association of loan officer failed");
                        Log.Warn("No loan officer information found for : " + userName + "Message:" + ex.Message);
                    }
                    Log.Debug($"... association of loan officer completed");
                };
                assignmentTasks.Add(loanOfficerTask());

                Func<Task> affinityTask = async () =>
                {
                    Log.Debug($"... association of affinity partner started");
                    try
                    {
                        Log.Debug($"... workflow for affinity partner started - only for invite reference id");
                        //new check user is invited by affinity partner then get the username of AP and team
                        affinityPartnerAdminUserName = await GetAssociatedUserInfoByRole(userName, Configuration.AffinityPartnerAdminRole);
                        assignmentTasks.Add(AssignTeam(applicationTeam, affinityPartnerAdminUserName, Team.affinitypartner.ToString()));
                    }
                    catch (Exception ex)
                    {
                        Log.Info($"... association of affinity partner failed");
                        Log.Warn("No affinityPartner Admin User information found for : " + userName + "Message:" + ex.Message);
                    }
                    Log.Debug($"... association of affinity partner completed");
                    
                };
                assignmentTasks.Add(affinityTask());
                
                await Task.WhenAll(assignmentTasks);
                Log.Debug($"... invoke MultiAssign");
                await AssignmentService.MultiAssign(entityType, entityId, applicationTeam);
            }
            else if (borroweruser != null)
            {
                Log.Debug($"... starting workflow for direct sign user");

                //need to add borrower into team :
                applicationTeam.Add(new AssignmentRequest()
                {
                    User = borroweruser.Username,
                    Role = borroweruser.Roles[0],
                    Team = Team.contributor.ToString(),
                    UserId = borroweruser.Id
                });
                Log.Debug($"... starting workflow for direct sign user");
                Log.Debug($"... invoke MultiAssign");
                await AssignmentService.MultiAssign(entityType, entityId, applicationTeam);
            }
            Log.Debug($"... completed Assign");
            return assignmentResponse;
        }

        public async Task<string> GetAssociatedUserInfoByRole(string userName, string role)
        {
            Log.Debug("Started GetAssociateUserInfoByRole... ");

            var userInfo = await GetUser(userName);
            if (userInfo != null && !string.IsNullOrEmpty(userInfo.InvitationReferenceId))
            {
                Log.Debug("... get loan officer details using InvitationReferenceId");
                return await GetAssociatedUser(userInfo.InvitationReferenceId, role);
            }
            else if (userInfo != null && !string.IsNullOrEmpty(userInfo.BrandedSiteReferenceId))
            {
                Log.Debug("... get loan officer details from brandedsitereferenceid");
                var loanOfficerObject = await UserProfile.GetProfileByUniqueCode(userInfo.BrandedSiteReferenceId);

                Log.Debug($"... invoke GetProfileByUniqueCode('{userInfo.BrandedSiteReferenceId}')");
                //if (loanOfficerObject != null && loanOfficerObject.Username != null && loanOfficerObject.Title.Equals(role, StringComparison.InvariantCultureIgnoreCase))
                if (loanOfficerObject != null && loanOfficerObject.Username != null)
                {
                    Log.Debug($"... lo for brandedsitereferenceid '{userInfo.BrandedSiteReferenceId}' is '{loanOfficerObject.Username}')");
                    return loanOfficerObject.Username.ToLower();
                }
                else
                {
                    return string.Empty;
                }

            }
            else
            {
                throw new ArgumentException("No associated user found for role : " + role);
            }
        }

        public async Task<string> GetAssociatedUserInfoByRoleForLead(string invitationReferenceId, string role)
        {
            return await GetAssociatedUser(invitationReferenceId, role);
        }

        private async Task<string> GetAssociatedUser(string invitationReferenceId, string role)
        {
            //now we have to find the invitation to identify that invitedby is match with the role.
            var invitation = await Get(invitationReferenceId);
            if (invitation != null)
            {
                //now need to get the invitedby email and identify the role is loan officer
                var invitedByRoles = await GetUserRoles(invitation.InvitedBy);
                if (invitedByRoles == null)
                    throw new ArgumentException("No associated user found for role : " + role);

                var loanOfficerRole = invitedByRoles.ToList().FirstOrDefault(x => x.ToLower() == role.ToLower());
                if (string.IsNullOrEmpty(loanOfficerRole))
                {
                    // This code is required as it will recursively call and get the assigned role user if it is there                    
                    return await GetAssociatedUserInfoByRole(invitation.InvitedBy, role);
                }
                else
                    return invitation.InvitedBy;
            }
            else
            {
                throw new ArgumentException("No associated user found for role : " + role);
            }
        }

        public async Task<IInvite> Get(string inviteReferenceId)
        {
            var inviteData = await Repository.GetInvite(inviteReferenceId);
            if (inviteData == null)
                throw new ArgumentException("Invitation not found.");

            return inviteData;
        }

        public async Task<IInviteVM> GetInviteData(string key, string value)
        {
            IInviteVM inviteDataVM = new InviteVM();
            if (key.ToLower().Equals("refid"))
            {
                var inviteData = await Repository.GetInvite(value);

                if (inviteData == null)
                    throw new ArgumentException("Invitation not found.");

                string loanOfficerUserName = string.Empty;
                try
                {
                    loanOfficerUserName = await GetAssociatedUser(inviteData.InvitationToken, Configuration.LoanOfficerRole);
                }
                catch (Exception ex)
                {
                    Log.Debug(ex.Message, ex);
                }
                string affinityPartNerUserName = string.Empty;

                try
                {
                    affinityPartNerUserName = await GetAssociatedUser(inviteData.InvitationToken, Configuration.AffinityPartnerAdminRole);
                }
                catch (Exception ex)
                {
                    Log.Debug(ex.Message, ex);
                }

                IUserProfile loanOfficerProfile = null, AffinityPartnerProfile = null;
                LendFoundry.Security.Identity.IUser loanOfficerUser = null, AffinityPartnerUser = null;
                var taskList = new List<Task>();
                if (!string.IsNullOrEmpty(loanOfficerUserName))
                {

                    Func<Task> LOProfileTask = async () =>
                    {
                        try
                        {
                            loanOfficerProfile = await UserProfile.GetProfile(loanOfficerUserName);
                        }
                        catch (Exception ex)
                        {
                            Log.Error($"Profile data not found for  {loanOfficerUserName}", ex);
                        }
                    };
                    taskList.Add(LOProfileTask());

                    Func<Task> LOUserTask = async () =>
                    {
                        try
                        {
                             loanOfficerUser = await GetUser(loanOfficerUserName);
                        }
                        catch (Exception ex)
                        {
                            Log.Error($"User data not found for {loanOfficerUserName}", ex);
                        }
                    };
                    taskList.Add(LOUserTask());
                }

                if (!string.IsNullOrEmpty(affinityPartNerUserName))
                {

                     Func<Task> affinityProfileTask = async () =>
                    {
                        try
                        {
                             AffinityPartnerProfile = await UserProfile.GetProfile(affinityPartNerUserName);
                        }
                        catch (Exception ex)
                        {
                            Log.Error($"User data not found for {affinityPartNerUserName}", ex);
                        }
                    };
                    taskList.Add(affinityProfileTask());

                    Func<Task> affinityUserTask = async () =>
                    {
                        try
                        {
                             AffinityPartnerUser = await GetUser(affinityPartNerUserName);
                        }
                        catch (Exception ex)
                        {
                            Log.Error($"User data not found for {affinityPartNerUserName}", ex);
                        }
                    };
                    taskList.Add(affinityUserTask());
                }

                 await Task.WhenAll(taskList);

                var onjInviteByInfo = new List<InvitedBy>();

                if (loanOfficerProfile != null)
                {
                    var invitedBy = new InvitedBy();
                    invitedBy.Username = loanOfficerProfile.Username;
                    invitedBy.Email = loanOfficerProfile.Email;
                    invitedBy.FirstName = loanOfficerProfile.FirstName;
                    invitedBy.LastName = loanOfficerProfile.LastName;
                    invitedBy.PhoneNumber = !string.IsNullOrEmpty(loanOfficerProfile.Phone) ? loanOfficerProfile.Phone : loanOfficerProfile.MobileNumber;
                    if(loanOfficerUser != null)
                    {
                        invitedBy.Role =loanOfficerUser.Roles.FirstOrDefault(x => x.Contains(Configuration.LoanOfficerRole));
                    }
                    else
                        invitedBy.Role = Configuration.LoanOfficerRole;

                    invitedBy.NmlsNumber = loanOfficerProfile.NmlsNumber;
                    invitedBy.isLoanOfficer = true;
                    onjInviteByInfo.Add(invitedBy);
                }

                if (AffinityPartnerProfile != null)
                {
                    var invitedBy = new InvitedBy();
                    invitedBy.Username = AffinityPartnerProfile.Username;
                    invitedBy.Email = AffinityPartnerProfile.Email;
                    invitedBy.FirstName = AffinityPartnerProfile.FirstName;
                    invitedBy.LastName = AffinityPartnerProfile.LastName;
                    invitedBy.PhoneNumber = !string.IsNullOrEmpty(AffinityPartnerProfile.Phone) ? AffinityPartnerProfile.Phone : AffinityPartnerProfile.MobileNumber;
                     if(AffinityPartnerUser != null)
                    {
                        invitedBy.Role =AffinityPartnerUser.Roles.FirstOrDefault(x => x.Contains(Configuration.AffinityPartnerAdminRole));
                    }
                    else
                        invitedBy.Role = Configuration.AffinityPartnerAdminRole;
                    //invitedBy.Role = AffinityPartnerProfile.Title;
                    invitedBy.NmlsNumber = AffinityPartnerProfile.NmlsNumber;
                    onjInviteByInfo.Add(invitedBy);
                }
                inviteDataVM.InvitedBy = onjInviteByInfo;
                inviteDataVM.InviteUserName = inviteData.UserName;
                inviteDataVM.InviteEmail = inviteData.InviteEmail;
                inviteDataVM.InviteMobileNumber = inviteData.InviteMobileNumber;
                inviteDataVM.InviteFirstName = inviteData.InviteFirstName;
                inviteDataVM.InviteLastName = inviteData.InviteLastName;
                inviteDataVM.Role = inviteData.Role;
                inviteDataVM.Team = inviteData.Team;
            }
            else
            {
                //When redirected form brandedsite url then it is always loan officer brandedsite token.
                var userProfile = await UserProfile.GetProfileByUniqueCode(value);

                if (userProfile == null)
                    throw new ArgumentException($"No user profile found for the branded code {value}");

                var userData = await GetUser(userProfile.Username);

                var onjInviteByInfo = new List<InvitedBy>();
                InvitedBy invitedBy = new InvitedBy();
                invitedBy.Email = userProfile.Email;
                invitedBy.Username = userProfile.Username;
                invitedBy.FirstName = userProfile.FirstName;
                invitedBy.LastName = userProfile.LastName;
                invitedBy.PhoneNumber = !string.IsNullOrEmpty(userProfile.Phone) ? userProfile.Phone : userProfile.MobileNumber;
                
                invitedBy.NmlsNumber = userProfile.NmlsNumber;
                if(userData != null)
                {
                    var role = userData.Roles.FirstOrDefault(x => x.Contains(Configuration.LoanOfficerRole));
                    invitedBy.Role =role;
                    //invitedBy.Role = userProfile.Title;
                    //if (Configuration.LoanOfficerRole.Equals(userProfile.Title, StringComparison.InvariantCultureIgnoreCase))
                    //    invitedBy.isLoanOfficer = true;

                    if(Configuration.LoanOfficerRole.Equals(role))
                         invitedBy.isLoanOfficer = true;
                }
                onjInviteByInfo.Add(invitedBy);
                inviteDataVM.InvitedBy = onjInviteByInfo;
            }

            return inviteDataVM;
        }

        /// <summary>
        /// GetInviteInformation
        /// </summary>
        /// <param name="userName"></param>
        /// <returns></returns>
        public async Task<IInvite> GetInviteByUser(string userName)
        {
            if (string.IsNullOrWhiteSpace(userName))
                throw new ArgumentException("UserId is null or empty.");

            var inviteData = await Repository.GetInviteByUser(userName);
            if (inviteData == null)
                throw new ArgumentException("No record found.");

            return inviteData;
        }

        public async Task<IInvite> VerifyToken(string entityType, IInviteVerifyRequest inviteVerifyRequest)
        {
            Validator.EnsureEntityType(entityType);
            var inviteData = await Repository.GetInvite(entityType, inviteVerifyRequest.InviteRefId);
            if (inviteData == null)
                throw new ArgumentException("Invitation not found.");

            if (inviteData.InvitationTokenExpiry < DateTime.UtcNow)
                throw new ArgumentException("Invitation token expired.");

            if (!string.IsNullOrEmpty(inviteVerifyRequest.InviteEmail))
            {
                if (!inviteData.InviteEmail.Equals(inviteVerifyRequest.InviteEmail, StringComparison.InvariantCultureIgnoreCase))
                    throw new ArgumentException("Invitation email does not match.");
            }
            else if (!string.IsNullOrEmpty(inviteVerifyRequest.Username))
            {
                var user = await IdentityService.GetUser(inviteVerifyRequest.Username);
                if (user != null)
                {
                    if (!inviteData.InviteEmail.Equals(user.Email, StringComparison.InvariantCultureIgnoreCase))
                        throw new ArgumentException("Invitation email does not match.");
                }
                else
                {
                    throw new ArgumentException("Username not found.");
                }
            }

            return inviteData;
        }

        private async Task UpdateInviteSetRelationship(string invitedBy, string inviteUserName, string inviteRole)
        {
            Log.Debug("Started UpdateInviteSetRelationship...");

            var invitedByData = await UserProfile.GetProfile(invitedBy);

            if (invitedByData.Relationships == null)
                invitedByData.Relationships = new List<UserRelationship>();

            Log.Debug("Update only if user is not into relationship");
            var isAlreadyRelated = invitedByData.Relationships.Any(x => x.MemberName.Equals(inviteUserName) && x.MemberRole.Equals(inviteRole));

            if (!isAlreadyRelated)
            {
                invitedByData.Relationships.Add(new UserRelationship()
                {
                    MemberName = inviteUserName,
                    MemberRole = inviteRole
                });
                Log.Debug("...UpdateUserRelationship");
                await UserProfile.UpdateUserRelationship(invitedByData.Username, invitedByData.Relationships);
            }

            Log.Debug("Update CompanyId if not null...");
            if (!string.IsNullOrEmpty(invitedByData.CompanyId))
            {
                var userProfile = await UserProfile.GetProfile(inviteUserName);

                if (userProfile != null && userProfile.CompanyId == null)
                {
                    userProfile.CompanyId = invitedByData.CompanyId;
                    Log.Debug("...UpdateProfile for CompanyId");
                    await UserProfile.UpdateProfile(userProfile.Username, userProfile);
                }
            }
        }

        private async Task UpdateInviteUserProfile(IInvite inviteData, IInviteUpdateRequest updateRequest)
        {
            Log.Debug("Started UpdateInviteUserProfile...");
            try
            {
                /*Copy FirstName, LastName, Mobile Number from Invite to UserProfile*/
                if (!string.IsNullOrEmpty(inviteData.InviteFirstName) && !string.IsNullOrEmpty(inviteData.InviteLastName))
                {
                    Log.Debug($"... Get Profile for user - {updateRequest.UserName}");
                    var userProfile = await UserProfile.GetProfile(updateRequest.UserName);
                    if (userProfile != null &&
                        (string.IsNullOrEmpty(userProfile.FirstName) && string.IsNullOrEmpty(userProfile.LastName)))
                    {
                        userProfile.FirstName = inviteData.InviteFirstName;
                        userProfile.LastName = inviteData.InviteLastName;
                        userProfile.MobileNumber = inviteData.InviteMobileNumber;
                        Log.Debug("...Update UserProfile : FirstName, LastName and MobileNumber");
                        await UserProfile.UpdateProfile(updateRequest.UserName, userProfile);
                    }
                }

                var isAffinityPartner = inviteData.Team.ToString().Equals(Team.affinitypartner.ToString());
                var isAdminRole = inviteData.Role.ToString().Equals(Configuration.AffinityPartnerAdminRole);

                /*Check if invite is for Affinity Partner*/
                if (isAffinityPartner)
                {
                    /*Check if invite is not for Admin*/
                    if (!isAdminRole)
                    {
                        var userName = inviteData.InvitedBy;
                        Log.Debug("...Update UpdateInviteSetRelationship");
                        await UpdateInviteSetRelationship(userName, updateRequest.UserName, inviteData.Role);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error("Error in UpdateInviteUserProfile : ", ex);
            }
        }
        public async Task AddMultiAssignmentAfterLogin(string entityType, IInviteUpdateRequest updateRequest)
        {           
            EnsureHelper.EnsureUpdateInvite(updateRequest);
            entityType = Validator.EnsureEntityType(entityType);
            var inviteData = await Repository.GetInviteByUser(updateRequest.UserName);
            if (inviteData == null)
                throw new ArgumentException("Invalid Invitation.");
            if (inviteData.ReferenceApplicationNumbers != null)
            {
                if (!string.IsNullOrEmpty(updateRequest.EmailAddress))
                {
                    if (!inviteData.InviteEmail.Equals(updateRequest.EmailAddress, StringComparison.InvariantCultureIgnoreCase))
                        throw new ArgumentException("Invitation email does not match.");
                }
                await UpdateInviteAssignment(inviteData, entityType, updateRequest);
            }
        }
        private async Task UpdateInviteAssignment(IInvite inviteData, string entityType, IInviteUpdateRequest updateRequest)
        {
            Log.Debug("Started UpdateInviteAssignment...");
            //Now add this invitation user to assignment table
            bool isLenderMember = inviteData.Team.Equals(Team.lender);

            if (!isLenderMember)
            {
                //only borrow invite that time assing user with borrower application
                try
                {
                    var objRequest = new AssignmentRequest()
                    {
                        UserId = updateRequest.UserId,
                        User = updateRequest.UserName,
                        Role = inviteData.Role,
                        Team = inviteData.Team.ToString()
                    };
                    Log.Debug("...Assign");
                    if (inviteData.ReferenceApplicationNumbers != null)
                    {
                        inviteData.ReferenceApplicationNumbers.Add(inviteData.EntityId);
                        foreach (var entityId in inviteData.ReferenceApplicationNumbers)
                        {
                            try
                            {
                                Log.Debug($"Start assign for {entityId}");
                                await AssignmentService.Assign(entityType, entityId, objRequest);
                                Log.Debug($"End assign for {entityId}");
                            }
                            catch (InvalidOperationException)
                            {
                                Log.Debug($"...user already associated with application for {entityId}");
                            }
                        }
                    }
                    else
                    {
                        await AssignmentService.Assign(entityType, inviteData.EntityId, objRequest);
                    }
                }
                catch (InvalidOperationException)
                {
                    Log.Debug("...user already associated with application");
                }
            }
        }

        public async Task<bool> UpdateInviteeNameDetails(string inviteId, string inviteFirstName, string inviteLastName)
        {
            Log.Debug("Started UpdateInviteeNameDetails...");
            if (string.IsNullOrEmpty(inviteId) || string.IsNullOrEmpty(inviteFirstName) || string.IsNullOrEmpty(inviteLastName))
                throw new ArgumentException("Invalid Invitation Details.");
            
            return await Repository.UpdateInviteeNameDetails(inviteId, inviteFirstName, inviteLastName);
        }

        public async Task<IInvite> UpdateInvite(string entityType, IInviteUpdateRequest updateRequest)
        {
            Log.Debug("Started UpdateInvite...");
            EnsureHelper.EnsureUpdateInvite(updateRequest);
            entityType = Validator.EnsureEntityType(entityType);
            var inviteData = await Repository.GetInvite(entityType, updateRequest.InviteRefId);

            if (inviteData == null)
                throw new ArgumentException("Invalid Invitation.");

            if (!string.IsNullOrEmpty(updateRequest.EmailAddress))
            {
                if (!inviteData.InviteEmail.Equals(updateRequest.EmailAddress, StringComparison.InvariantCultureIgnoreCase))
                    throw new ArgumentException("Invitation email does not match.");
            }

            /*
            * Update First&Last Name : 
            * After signup firstname and last name has to be updated from invite into user profile
            */

            Log.Debug($"... processing for username -{updateRequest.UserName}");
            Log.Debug("...UpdateInviteUserProfile");
            await UpdateInviteUserProfile(inviteData, updateRequest);

            if (inviteData.InvitationTokenExpiry < DateTime.UtcNow)
                throw new ArgumentException("Invitation token expired.");

            if (!(string.IsNullOrEmpty(inviteData.UserId) && string.IsNullOrEmpty(inviteData.UserName)))
                throw new ArgumentException("Invitation already completed.");

            inviteData.UserName = updateRequest.UserName;
            inviteData.UserId = updateRequest.UserId;
            inviteData.InvitationAcceptedDate = TenantTime.Now;
            inviteData.InvitationTokenExpiry = DateTime.UtcNow;
            Log.Debug("...Update Invite : UserName, UserId");
            Repository.Update(inviteData);

            Log.Debug("...UpdateInviteAssignment");
            await UpdateInviteAssignment(inviteData, entityType, updateRequest);

            Log.Debug("...Publish InvitationAccepted");
            await EventHub.Publish($"{Validator.EnsureFirstCharacterCapital(entityType)}{nameof(InvitationAccepted)}", new InvitationSend(inviteData));
            return inviteData;
        }

        /// <summary>
        /// Get the lead or invitation details where user has not signup 
        /// </summary>
        /// <returns>List of invite data</returns>

        public async Task<IEnumerable<IInvite>> GetAllLeadInvite()
        {
            string inviteLeadRole = Configuration.InviteLeadRole;
            if (string.IsNullOrEmpty(inviteLeadRole))
                throw new ArgumentException("InviteLead Role configuration not found.");
            return await Repository.GetAllLeadInvite(inviteLeadRole);
        }

        /// <summary>
        /// Get the lead or invitation details where user has not signup and invited by supplied invitedby email
        /// </summary>
        /// <param name="invitedEmail">invited by Email (Email address)</param>
        /// <returns>List of invite information</returns>
        public async Task<IEnumerable<IInvite>> GetAllLeadInvitedBy(string invitedByEmail)
        {
            if (string.IsNullOrWhiteSpace(invitedByEmail))
                throw new ArgumentException("invitedByEmail is null or empty.");

            string inviteLeadRole = Configuration.InviteLeadRole;
            if (string.IsNullOrEmpty(inviteLeadRole))
                throw new ArgumentException("InviteLead Role configuration not found.");

            invitedByEmail = invitedByEmail.DocittUrlDecode();

            return await Repository.GetAllLeadInvitedBy(invitedByEmail, inviteLeadRole);
        }

        // <summary>
        /// Get the lead or invitation details where supplied invitee email
        /// </summary>
        /// <param name="inviteeEmail">invitee Email</param>
        /// <returns>invite/lead information</returns>
        public async Task<IInvite> GetLeadInviteInformationByInvitee(string inviteeEmail)
        {
            if (string.IsNullOrWhiteSpace(inviteeEmail))
                throw new ArgumentException("inviteeEmail is null or empty.");

            inviteeEmail = inviteeEmail.DocittUrlDecode();

            return await Repository.GetLeadInviteInformationByInvitee(inviteeEmail);
        }

        // <summary>
        /// send reminder
        /// </summary>
        /// <param name="invitationId">invitation Id</param>
        /// <returns>Updated invitation info</returns>
        public async Task<IInvite> SendReminder(string invitationId, bool sendEmail = true, string entityId = "")
        {
            if (string.IsNullOrWhiteSpace(invitationId))
                throw new ArgumentException("invitationId is null or empty.");

            var inviteData = await Repository.GetInviteById(invitationId);
            if (inviteData == null)
                throw new ArgumentException("Invalid Invitation.");
            if (!string.IsNullOrWhiteSpace(entityId))
            {
                if (inviteData.ReferenceApplicationNumbers == null)
                {
                    inviteData.ReferenceApplicationNumbers = new List<string>();
                }
                if (!inviteData.ReferenceApplicationNumbers.Any(p => p.Equals(entityId)))
                {
                    inviteData.ReferenceApplicationNumbers.Add(entityId);
                }
            }

            if (inviteData != null && inviteData.InvitationTokenExpiry != null)
            {
                var inviteRequest = new InviteRequest()
                {
                    InvitedBy = inviteData.InvitedBy.ToLower(),
                    InviteEmail = inviteData.InviteEmail.ToLower(),
                    InviteFirstName = inviteData.InviteFirstName,
                    InviteLastName = inviteData.InviteLastName,
                    InviteMobileNumber = inviteData.InviteMobileNumber,
                    Role = inviteData.Role,
                    Team = inviteData.Team.ToString()
                };

                if (inviteData.InvitationTokenExpiry < DateTime.UtcNow)
                {
                    var invitationTokenExpiry = DateTime.UtcNow.AddMinutes(Configuration.TokenTimeout);
                    //var invitationToken = $"{Guid.NewGuid():N}{Guid.NewGuid():N}";
                    //inviteData.InvitationToken = invitationToken;
                    inviteData.InvitationTokenExpiry = invitationTokenExpiry;
                }                
                await SaveInviteDataAndSendEmail(inviteData.EntityType,string.IsNullOrWhiteSpace( entityId)? inviteData.EntityId : entityId, inviteRequest, inviteData, sendEmail, true);
            }

            return inviteData;
        }


        /// <summary>
        /// Save InviteData and send Email 
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="inviteRequest"></param>
        /// <param name="inviteData"></param>
        /// <param name="sendEmail">email flag</param>
        /// <returns></returns>
        private async Task SaveInviteDataAndSendEmail(string entityType, string entityId, IInviteRequest inviteRequest, IInvite inviteData, bool sendEmail, bool isUpdate = false)
        {
            Log.Debug("Started SaveInviteDataAndSendEmail");
            var invitationUrl = string.Empty;
            var invitationTemplateName = string.Empty;
            var invitationTemplateVersion = string.Empty;
            var TextMessageTemplateName = string.Empty;
            var TextMessageTemplateVersion = string.Empty;
            bool isUserExist = false;

            Log.Debug("Check if the user already exists.");

            try
            {
                var userData = await UserProfile.GetProfile(inviteRequest.InviteFirstName, inviteRequest.InviteLastName, inviteRequest.InviteEmail);
                if (userData != null)
                {
                    Log.Debug("... user data found in profile");
                    isUserExist = true;
                }
                else
                {
                    Log.Debug("... user data not found in profile");
                }
            }
            catch (Exception)
            {
                isUserExist = false;
            }

            // Borrwer and Co-Borrower
            if (Configuration.BorrowerRoles.Any(x => x.ToLower() == inviteRequest.Role.ToLower()))
            {
                if (isUserExist)
                {
                    invitationUrl = Configuration.BorrowerLoginPage;
                }
                else
                    invitationUrl = Configuration.BorrowerSignupPage;

                invitationTemplateName = Configuration.BorrowerInvitationTemplate.Name;
                invitationTemplateVersion = Configuration.BorrowerInvitationTemplate.Version;
                TextMessageTemplateName = Configuration.BorrowerTextMessageTemplate.Name;
                TextMessageTemplateVersion = Configuration.BorrowerTextMessageTemplate.Version;

            }
            // Affinity Partner invite by borrower
            else if (inviteRequest.Team.Equals(Team.contributor.ToString()))
            {
                if (isUserExist)
                {
                    invitationUrl = Configuration.AffinityPartnerLoginPage;
                }
                else
                {
                    invitationUrl = Configuration.AffinityPartnerSignupPage;
                }
                invitationTemplateName = Configuration.AffinityPartnerInvitationTemplate.Name;
                invitationTemplateVersion = Configuration.AffinityPartnerInvitationTemplate.Version;
                TextMessageTemplateName = Configuration.AffinityPartnerTextMessageTemplate.Name;
                TextMessageTemplateVersion = Configuration.AffinityPartnerTextMessageTemplate.Version;
            }

            // Affinity Partner by lender
            else if (inviteRequest.Team.Equals(Team.affinitypartner.ToString()))
            {
                if (isUserExist)
                {
                    invitationUrl = Configuration.AffinityPartnerLoginPage;
                }
                else
                {
                    invitationUrl = Configuration.AffinityPartnerSignupPage;
                }
                invitationTemplateName = Configuration.AffinityPartnerInvitationTemplate.Name;
                invitationTemplateVersion = Configuration.AffinityPartnerInvitationTemplate.Version;
                TextMessageTemplateName = Configuration.AffinityPartnerTextMessageTemplate.Name;
                TextMessageTemplateVersion = Configuration.AffinityPartnerTextMessageTemplate.Version;
            }

            // Lender
            else if (inviteRequest.Team.Equals(Team.lender.ToString()))
            {
                if (isUserExist)
                {
                    invitationUrl = Configuration.BackOfficeLoginPage;
                }
                else
                {
                    invitationUrl = Configuration.BackOfficeSignupPage;
                }
                invitationTemplateName = Configuration.LenderInvitationTemplate.Name;
                invitationTemplateVersion = Configuration.LenderInvitationTemplate.Version;
            }

            if (!string.IsNullOrEmpty(invitationUrl))
            {
                Log.Debug("... setting the invitation url");
                inviteData.InvitationUrl = string.Format(invitationUrl, inviteData.InvitationToken);
            }
            if (!isUpdate)
            {
                Log.Debug("... save invite data");
                Repository.Add(inviteData);

                Log.Debug("... invoke event InvitationSend");
                //ApplicationInvitationSend Event
                inviteData.EntityId = entityId;
                await EventHub.Publish($"{Validator.EnsureFirstCharacterCapital(entityType)}{nameof(InvitationSend)}",
                    new InvitationSend(inviteData));
            }
            else
            {
                Log.Debug("... update invite data"); //for the lead screen there is reminder functionality so need to update the invite token incase of expiry
                inviteData.LastReminder = TenantTime.Now;
                Repository.Update(inviteData);

                Log.Debug("... invoke event InvitationReminderSend");
                //InvitationReminderSend Event
                inviteData.EntityId = entityId;
                await EventHub.Publish($"{nameof(InvitationReminderSend)}", new InvitationSend(inviteData));
            }

            if (string.IsNullOrWhiteSpace(invitationUrl))
            {
                Log.Debug("... invitation url is not available");
                throw new ArgumentException($"Error : Invite Email can not be send to {inviteRequest.InviteEmail} as invitationUrl is not available");
            }
            var taskList = new List<Task>();
            if (sendEmail)
            {
                IUserProfile userProfile = null;

                if (!string.IsNullOrWhiteSpace(inviteRequest.InvitedBy))
                {
                    try
                    {
                        userProfile = await UserProfile.GetProfile(inviteRequest.InvitedBy);
                    }
                    catch (Exception ex)
                    {
                        Log.Error($"UserProfile not found for {inviteRequest.InvitedBy}", ex);
                    }
                }

                Log.Debug("... invoke email service");

                Func<Task> emailTask = async () =>
                {
                    try
                    {
                        await EmailService.Send(
                        entityType,
                        entityId,
                        invitationTemplateName,
                        new
                        {
                            inviteRequest.InvitedBy,
                            inviteRequest.InviteEmail,
                            inviteRequest.InviteFirstName,
                            inviteRequest.InviteLastName,
                            Email = inviteRequest.InviteEmail,
                            invitationUrl = inviteData.InvitationUrl,
                            InvitedByFirstName = userProfile?.FirstName,
                            InvitedByLastName = userProfile?.LastName,
                            inviteRequest.Role
                        });
                    }
                    catch (Exception ex)
                    {
                        Log.Error($"SaveInviteDataAndSendEmail : Error during send email. Error : {ex.Message}");
                    }
                };
                taskList.Add(emailTask());               
            }
            else
            {
                Log.Info($"... email is set not to be send to {inviteRequest.InviteEmail} from docitt");
            }

            if (inviteRequest.SendBothEmailText)
            {              
                var objData = new { invitationUrl = inviteData.InvitationUrl };
                var templateResult = await TemplateManager.ProcessActiveTemplate(TextMessageTemplateName, Format.Text, objData);
                var textMessageRequest = new TextMessageRequest();
                textMessageRequest.CountryCode = inviteRequest.CountryCode;
                textMessageRequest.PhoneNumber = inviteRequest.InviteMobileNumber;
                if (templateResult != null)
                    textMessageRequest.Body = templateResult.Data;
                else
                    textMessageRequest.Body = string.Empty;

                Func<Task> textTask = async () =>
                {
                    try
                    {
                        await MessageService.SendTextMessage(textMessageRequest);
                    }
                    catch (Exception ex)
                    {
                        Log.Error($"Text Message Error {inviteRequest.InvitedBy}({inviteRequest.InviteMobileNumber})", ex);
                    }
                };
                taskList.Add(textTask());
            }
            else
            {
                Log.Info($"... sms is not to be send to {inviteRequest.InvitedBy}({inviteRequest.InviteMobileNumber}) as sendbothemailtext is send as false.");
            }
            
            await Task.WhenAll(taskList);
        }

        public async Task<bool> ApplicationReminderNotification(string entityType, string entityId, string templateName, ApplicationReminderNotification payload)
        {
            Log.Debug("ApplicationReminderNotification... Validate EntityId ");
            Validator.EnsureEntityId(entityId);
            payload.EntityId = entityId;
            var alloyConfiguration = GetAlloyConfiguration();
            if(alloyConfiguration != null && !string.IsNullOrEmpty(alloyConfiguration.Login))
                payload.BorrowerPortalUrl = alloyConfiguration.Login;
            
            Log.Debug($"Get Loan Officer Profile");
            var loUser = await AssignmentService.GetByRole(entityType, entityId, "Loan Officer");
            if(loUser != null)
            {
                var loProfile = await UserProfile.GetProfile(loUser.Assignee);
                if(loProfile != null)
                {
                    payload.LoanOfficerName = loProfile.FirstName + " " + loProfile.LastName;
                    payload.LoanOfficerNMLS = loProfile.NmlsNumber;
                    payload.LoanOfficerPhoneNumber = loProfile.Phone;
                    payload.LoanOfficerEmail = loProfile.Email;
                }
                else
                {
                    Log.Debug($"Loan Officer not assigned for application {entityId}");
                }
            }

            var currentUser = Validator.EnsureCurrentUser();
            Log.Debug($"Get Initiator Profile {currentUser}");
            var currentUserProfile = await UserProfile.GetProfile(currentUser);
            if(currentUserProfile != null)
            {
                payload.InitiatedByName = currentUserProfile.FirstName + " " + currentUserProfile.LastName;
                payload.InitiatedByPhoneNumber = currentUserProfile.Phone; 
                payload.InitiatedByEmail = currentUserProfile.Email;
            }

            // Raise notification ApplicationReminderNotification
            await EventHub.Publish($"{nameof(ApplicationReminderNotification)}", payload);
            // Call Email Service
            await EmailService.Send(entityType, entityId, templateName, payload);
            return true;
        }

        public async Task<IInvite> SendInvite(string entityType, string entityId, IInviteRequest inviteRequest, bool sendEmail = true)
        {
            Log.Debug("SendInvite... ");
            EnsureHelper.EnsureCreateInvite(inviteRequest);
            Validator.EnsureEntityId(entityId);
            entityType = Validator.EnsureEntityType(entityType);
            inviteRequest.InviteEmail = Validator.EnsureEmailAddress(inviteRequest.InviteEmail);

            var invitationTokenExpiry = DateTime.UtcNow.AddMinutes(Configuration.TokenTimeout);
            var invitationToken = $"{Guid.NewGuid():N}{Guid.NewGuid():N}";

            //invitation is expired and user is not signed up then we have to just update/extend the invitation token expiry.
            bool isUpdate = false;

            //Check user is already invited or already completed the invitation then it should not be invited again.
            var inviteInfo = await Repository.GetInvite(entityType, entityId, inviteRequest);

            if (inviteInfo != null && inviteInfo.InvitationTokenExpiry != null)
            {
                if (!inviteRequest.isLenderNewLoan)
                {
                    if (inviteInfo.UserId != null && inviteInfo.UserName != null)
                    {
                        Log.Debug("... UserId and UserName is created");
                        throw new ArgumentException("User already invited.");
                    }
                }

                if (inviteInfo.InvitationTokenExpiry > DateTime.UtcNow)
                {
                    Log.Debug("... InvitationToken is not expired");
                    throw new ArgumentException("User already invited.");
                }
                else
                {
                    Log.Debug("... InvitationToken is expired.so extend the invitation expiry time.");
                    //invitation is expired and user is not signed up then we have to just update/extend the invitation token expiry.
                    //in case invited again
                    if (string.IsNullOrEmpty(inviteInfo.UserId))
                    {
                        isUpdate = true;
                        inviteInfo.InvitationTokenExpiry = invitationTokenExpiry;
                    }
                }
            }
            IInvite inviteData = new Invite
            {
                EntityType = entityType,
                EntityId = entityId,
                InvitedBy = inviteRequest.InvitedBy.ToLower(),
                InviteEmail = inviteRequest.InviteEmail.ToLower(),
                InviteFirstName = inviteRequest.InviteFirstName,
                InviteLastName = inviteRequest.InviteLastName,
                InviteMobileNumber = inviteRequest.InviteMobileNumber,
                InvitationDate = TenantTime.Now,
                Role = inviteRequest.Role,
                InvitationToken = invitationToken,
                InvitationTokenExpiry = invitationTokenExpiry,
                Team = (Team)Enum.Parse(typeof(Team), inviteRequest.Team)
            };

            // Save and Send Invite Email
            // Save and Send Invite Email
            if (!isUpdate)
            {
                await SaveInviteDataAndSendEmail(entityType, entityId, inviteRequest, inviteData, sendEmail);
            }
            else
            {
                await SaveInviteDataAndSendEmail(entityType, entityId, inviteRequest, inviteInfo, sendEmail, isUpdate);
                inviteData = inviteInfo;
            }

            return inviteData;
        }

        private async Task<LendFoundry.Security.Identity.IUser> GetUser(string userName)
        {
            return await IdentityService.GetUser(userName);
        }

        private async Task<IEnumerable<string>> GetUserRoles(string userName)
        {
            return await IdentityService.GetUserRoles(userName);
        }

        private async Task AssignTeam(List<AssignmentRequest> applicationTeam, string loanOfficerUserName, string team)
        {
            //need to add loanofficer :
            var loanOfficerUser = await GetUser(loanOfficerUserName);
            applicationTeam.Add(new AssignmentRequest()
            {
                User = loanOfficerUserName,
                Role = loanOfficerUser.Roles[0],
                Team = team,
                UserId = loanOfficerUser.Id
            });

            //loan officer user is found now get the team of loan officer using user profile --> relationship.
            var loanOfficerTeam = await UserProfile.GetUserRelationships(loanOfficerUserName);
            if (loanOfficerTeam != null)
            {
                foreach (var itrTeam in loanOfficerTeam)
                {
                    //get the userid of user from identity service.
                    var memberUser = await GetUser(itrTeam.MemberName);
                    if (memberUser != null)
                    {
                        applicationTeam.Add(new AssignmentRequest()
                        {
                            User = itrTeam.MemberName.ToLower(),
                            Role = itrTeam.MemberRole,
                            Team = team,
                            UserId = memberUser.Id
                        });
                    }

                }
            }
        }

        public async Task<bool> ExpireInvitation(string invitationId)
        {
            Log.Debug("ExpireInvitation... ");

            var invitationTokenExpiry = DateTime.UtcNow;

            var inviteInfo = await Repository.GetInviteById(invitationId);

            if (inviteInfo != null && inviteInfo.InvitationTokenExpiry != null)
            {
                Log.Debug("... set token expiry in invite data as now");
                inviteInfo.InvitationTokenExpiry = DateTime.UtcNow;
                Repository.Update(inviteInfo);
            }

            return true;
        }

        public async Task<IEnumerable<IInvite>> GetInviteInfoByEmailAndRole(string entityType, string entityId, string invitedByEmail, string role)
        {
            Validator.EnsureEntityId(entityId);
            entityType = Validator.EnsureEntityType(entityType);
            if (string.IsNullOrWhiteSpace(invitedByEmail))
                throw new ArgumentException("invitedByEmail is null or empty.");
            if (string.IsNullOrEmpty(role))
                throw new ArgumentException("role is null or empty.");

            invitedByEmail = invitedByEmail.DocittUrlDecode();
            role = role.DocittUrlDecode();

            return await Repository.GetInviteInfoByEmailAndRole(entityType, entityId, invitedByEmail, role);
        }


        public async Task<bool> ExpireInvitations(IEnumerable<string> invitationIds)
        {
            Log.Debug("ExpireInvitation... Start");
            return await Repository.ExpireInvitations(invitationIds);
        }

        public async Task<IInvite> RegenerateInviteToken(string invitationId, string inviteEmailId)
        {
            Log.Debug($"RegenerateInviteToken... {invitationId} {inviteEmailId}");
            inviteEmailId = Validator.EnsureEmailAddress(inviteEmailId);
            var invitationToken = $"{Guid.NewGuid():N}{Guid.NewGuid():N}";

            var invitationTokenExpiry = DateTime.UtcNow.AddMinutes(Configuration.TokenTimeout);
            var inviteData = await Repository.RegenerateInviteToken(invitationId, inviteEmailId, invitationToken, invitationTokenExpiry);

            await EventHub.Publish($"{Validator.EnsureFirstCharacterCapital(inviteData.EntityType)}{nameof(RegenerateInvitation)}",
                    new RegenerateInvitation(inviteData));

            IUserProfile userProfile = null;

            if (!string.IsNullOrWhiteSpace(inviteData.InvitedBy))
            {
                try
                {
                    userProfile = await UserProfile.GetProfile(inviteData.InvitedBy);
                }
                catch (Exception ex)
                {
                    Log.Error($"UserProfile not found for {inviteData.InvitedBy}", ex);
                }
            }

            Log.Debug("... invoke email service");
            try
            {
                await EmailService.Send(
                    inviteData.EntityType,
                    inviteData.EntityId,
                    Configuration.BorrowerInvitationTemplate.Name,
                    new
                    {
                        inviteData.InvitedBy,
                        inviteData.InviteEmail,
                        inviteData.InviteFirstName,
                        inviteData.InviteLastName,
                        Email = inviteData.InviteEmail,
                        invitationUrl = inviteData.InvitationUrl,
                        InvitedByFirstName = userProfile?.FirstName,
                        InvitedByLastName = userProfile?.LastName,
                        inviteData.Role
                    });
            }
            catch (Exception ex)
            {
                Log.Error($"RegenerateInviteToken : Error during send email. Error : {ex.Message}");
            }

            return inviteData;
        }

        public Task<IInvite> GetInviteById(string invitationId)
        {
            return Repository.GetInviteById(invitationId);
        }

        private AlloyConfiguration GetAlloyConfiguration()
        {
            var serviceName = "alloy";
            var configurationService = ConfigurationServiceFactory.Create<AlloyConfiguration>(serviceName, TokenReader);
            var configuration = configurationService.Get();
            if (configuration == null)
                throw new Exception($"Configuration not found for '{serviceName}'");
            return configuration;
        }
    }
}
