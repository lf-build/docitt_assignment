﻿#if DOTNET2
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
#else
using Microsoft.AspNet.Builder;
using Microsoft.Framework.DependencyInjection;
#endif
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Docitt.AssignmentEngine
{
    public static class InviteListenerExtensions
    {
        public static void UseInviteListener(this IApplicationBuilder app)
        {
            app.ApplicationServices.GetRequiredService<IInviteListener>().Start();
        }
    }
}
