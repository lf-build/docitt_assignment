﻿using Docitt.AssignmentEngine.Services;
using System;
#if DOTNET2
using Microsoft.Extensions.DependencyInjection;
#else
using Microsoft.Framework.DependencyInjection;
#endif
using LendFoundry.Security.Tokens;
using LendFoundry.Foundation.Date;
using LendFoundry.EventHub;
using LendFoundry.Foundation.Logging;
using LendFoundry.Email.Client;

using LendFoundry.Configuration;

using LendFoundry.Security.Identity.Client;
using Docitt.UserProfile.Client;
using Docitt.TwilioOTP.Client;
using LendFoundry.TemplateManager.Client;

namespace Docitt.AssignmentEngine
{
    public class InviteServiceFactory : IInviteServiceFactory
    {
        public InviteServiceFactory(IServiceProvider provider)
        {
            if (provider == null) throw new ArgumentException($"{nameof(provider)} cannot be null");

            Provider = provider;
        }

        private IServiceProvider Provider { get; }

        public IInviteService Create(ITokenReader reader, ILogger logger, ITokenHandler tokenHandler)
        {
            var assignmentServiceFactory = Provider.GetService<IAssignmentServiceFactory>();
            var assignmentService = assignmentServiceFactory.Create(reader, logger);

            var repositoryFactory = Provider.GetService<IInviteRepositoryFactory>();
            var repository = repositoryFactory.Create(reader);

            var tenantTimeFactory = Provider.GetService<ITenantTimeFactory>();
            var configFactory = Provider.GetService<IConfigurationServiceFactory>();
            var tenantTime = tenantTimeFactory.Create(configFactory, reader);

            var eventHubFactory = Provider.GetService<IEventHubClientFactory>();
            var eventHub = eventHubFactory.Create(reader);

            var validatorFactory = Provider.GetService<IValidatorServiceFactory>();
            var validator = validatorFactory.Create(reader, logger);

            var emailServiceFactory = Provider.GetService<IEmailServiceFactory>();
            var emailService = emailServiceFactory.Create(reader);

            var assignmentConfigFactory = Provider.GetService<IConfigurationServiceFactory<AssignmentServiceConfiguration>>();
            var configuration = assignmentConfigFactory.Create(reader);

            var identityServiceFactory = Provider.GetService<IIdentityServiceFactory>();
           var identityService = identityServiceFactory.Create(reader);

            var userProfileServiceFactory = Provider.GetService<IUserProfileFactory>();
            var userProfileService = userProfileServiceFactory.Create(reader);

             var textMessageServiceFactory = Provider.GetService<ITextMessageServiceFactory>();
            var textMessageService = textMessageServiceFactory.Create(reader);

             var templateManagerServiceFactory = Provider.GetService<ITemplateManagerServiceFactory>();
            var templateManagerService = templateManagerServiceFactory.Create(reader);

            var configurationServiceFactory = Provider.GetService<IConfigurationServiceFactory>();
            var ensureHelper = new EnsureHelper();

            return new InviteService(
                assignmentService, 
                repository, 
                tenantTime, 
                eventHub, 
                validator, 
                emailService, 
                configuration.Get(),
                identityService, 
                userProfileService,
                ensureHelper,
                logger,
                textMessageService,
                templateManagerService,
                configurationServiceFactory,
                tokenHandler,
                reader
                );
        }
    }
}