﻿using LendFoundry.Foundation.Services;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using LendFoundry.Foundation.Date;
using LendFoundry.EventHub;
using Docitt.AssignmentEngine.Events;
using Docitt.AssignmentEngine.Services;


namespace Docitt.AssignmentEngine
{
    public class AssignmentService : IAssignmentService
    {
        public AssignmentService(IAssignmentRepository repository,  ITenantTime tenantTime, IEventHubClient eventHub,IValidatorService validator)
        {
            if (repository == null)
                throw new ArgumentException($"{nameof(repository)} is mandatory");
            if (eventHub == null)
                throw new ArgumentException($"{nameof(eventHub)} is mandatory");
            //if (identityService == null)
            //    throw new ArgumentException($"{nameof(identityService)} is mandatory");
            if (tenantTime == null)
                throw new ArgumentException($"{nameof(tenantTime)} is mandatory");
            if (validator == null)
                throw new ArgumentException($"{nameof(validator)} is mandatory");
            Repository = repository;
            //IdentityService = identityService;
            TenantTime = tenantTime;
            EventHub = eventHub;
            Validator = validator;           
        }


        private IAssignmentRepository Repository { get; }
        //private IIdentityService IdentityService { get; }
        private ITenantTime TenantTime { get; }
        private IEventHubClient EventHub { get; }
        private IValidatorService Validator { get; }
       

        public async Task<IAssignedResponse> IsAssigned(string entityType, string entityId)
        {
            Validator.EnsureEntityId(entityId);
            entityType = Validator.EnsureEntityType(entityType);
            var username = Validator.EnsureCurrentUser();

            var assignment = await Repository.GetByUserName(entityType, entityId, username);
            return assignment == null ? new AssignedResponse(false) : new AssignedResponse(true);
        }

        public async Task<IEnumerable<IAssignment>> Get(string entityType, string entityId)
        {
            Validator.EnsureEntityId(entityId);
            entityType = Validator.EnsureEntityType(entityType);

            return await Repository.Get(entityType, entityId);
        }

        public async Task<IEnumerable<IAssignment>> GetByUser(string entityType, string username)
        {
            if (string.IsNullOrWhiteSpace(username))
                throw new ArgumentException("username is required.", nameof(username));
            entityType = Validator.EnsureEntityType(entityType);
           

            return await Repository.GetByUserName(entityType, username);
        }

        public async Task<IAssignment> GetByRole(string entityType, string entityId, string role)
        {
            if (string.IsNullOrWhiteSpace(role))
                throw new ArgumentException("role is required.", nameof(role));

            Validator.EnsureEntityId(entityId);

            entityType = Validator.EnsureEntityType(entityType);

            return await Repository.GetByRole(entityType,entityId,role);
        }

        public async Task<IAssignment> Assign(string entityType, string entityId, IAssignmentRequest assignmentRequest)
        {
            var assignment = await Add(
                entityType, 
                entityId,
                assignmentRequest.User, 
                assignmentRequest.UserId, 
                assignmentRequest.Role, 
                assignmentRequest.Team);

            await EventHub.Publish($"{Validator.EnsureFirstCharacterCapital(entityType)}{nameof(AssignmentAssigned)}",new AssignmentAssigned(assignment));

            return assignment;
        }

        public async Task<IEnumerable<IAssignment>> MultiAssign(string entityType, string entityId, List<AssignmentRequest> assignmentRequest)
        {
            IList<IAssignment> objAssignment = new List<IAssignment>();

            foreach (var assign in assignmentRequest)
            {
                try
                {
                    var assignment = await Add(entityType, entityId, assign.User,assign.UserId, assign.Role, assign.Team);
                    await EventHub.Publish($"{Validator.EnsureFirstCharacterCapital(entityType)}{nameof(AssignmentAssigned)}", new AssignmentAssigned(assignment));
                    objAssignment.Add(assignment);
                }
                catch
                {

                }              
            }
            return objAssignment;
        }
        
        private async Task<IAssignment> Add(string entityType, string entityId, string username, string userId,string role, string team)
        {
            if (string.IsNullOrWhiteSpace(username))
                throw new ArgumentException("username is required.");

            Validator.EnsureEntityId(entityId);

            entityType = Validator.EnsureEntityType(entityType);

            //role = await Validator.EnsureUserRole(username, role);

            var assignment = await Repository.GetByUserName(entityType, entityId, username);
            if (assignment != null)
                throw new InvalidOperationException($"Assignment for {username} already exists.");
                       
            var assignedBy = string.Empty;
            IAssignment assignmentData = new Assignment
            {                
                AssignedOn = TenantTime.Now,
                AssignedBy = assignedBy,
                UserId = userId,
                Assignee = username.ToLower(),
                Role = role,
                EntityType = entityType,
                EntityId = entityId,
                IsActive = true,
                IsDisable = false,
                Team= (Team)Enum.Parse(typeof(Team), team)
            };

            Repository.Add(assignmentData);

            return assignmentData;
        }

        public async Task Unassign(string entityType, string entityId, string userName)
        {
            var assignment = await Remove(entityType, entityId, userName, true);
            await EventHub.Publish($"{Validator.EnsureFirstCharacterCapital(entityType)}{nameof(AssignmentUnassigned)}", new AssignmentUnassigned(assignment));
        }

        public async Task Release(string entityType, string entityId, string userName)
        {           
            var assignment =await  Remove(entityType, entityId, userName);
            await EventHub.Publish($"{Validator.EnsureFirstCharacterCapital(entityType)}{nameof(AssignmentReleased)}", new AssignmentReleased(assignment));
        }

        private async Task<IAssignment> Remove(string entityType, string entityId, string userName,bool isDisable = false)
        {
            if (string.IsNullOrWhiteSpace(userName))
                throw new ArgumentException("userName is required.", nameof(userName));

            Validator.EnsureEntityId(entityId);

            entityType = Validator.EnsureEntityType(entityType);

            var assignment = await Repository.GetByUserName(entityType, entityId, userName);
            if (assignment == null)
                throw new NotFoundException($"Assignment not found for the user {userName}");
            if (isDisable)
            {
                assignment.IsDisable = true;
            }
            else
            {
                var currentUser = Validator.EnsureCurrentUser();
                if (assignment.Assignee != currentUser)
                {
                    //var currentUserRoles = (await IdentityService.GetUserRoles(currentUser)).ToList();
                    //var childRoles = await IdentityService.GetChildRoles(currentUserRoles);
                    //if (!childRoles.Contains(assignment.Role, StringComparer.InvariantCultureIgnoreCase))
                    //    throw new InvalidOperationException($"Removal of assignment to {assignment.Role} is invalid.");
                }

                assignment.IsActive = false;
                assignment.IsDisable = true;
            }
         

            Repository.Update(assignment);
            return assignment;
        }
    }
}