﻿using System;

namespace Docitt.AssignmentEngine
{
    public class EnsureHelper : IEnsureHelper
    {

        public void EnsureCreateInvite(IInviteRequest inviteRequest)
        {
            if (inviteRequest == null)
                throw new ArgumentException($"{nameof(inviteRequest)} is mandatory");

            if (string.IsNullOrWhiteSpace(inviteRequest.InvitedBy) || string.IsNullOrEmpty(inviteRequest.InvitedBy))
                throw new ArgumentException("InviteBy is required.");

            if (string.IsNullOrWhiteSpace(inviteRequest.InviteEmail) || string.IsNullOrEmpty(inviteRequest.InvitedBy))
                throw new ArgumentException("Invite Email is required.");

            if (string.IsNullOrEmpty(inviteRequest.Role) || string.IsNullOrWhiteSpace(inviteRequest.Role))
                throw new ArgumentException("Role is required.");

            if (string.IsNullOrWhiteSpace(inviteRequest.Team) || string.IsNullOrEmpty(inviteRequest.Team))
                throw new ArgumentException("Team is required.");

            if (!Enum.IsDefined(typeof(Team), inviteRequest.Team))
                throw new ArgumentException("Team not found.");
            
        }

        public void EnsureUpdateInvite(IInviteUpdateRequest updateRequest)
        {
            if (updateRequest == null)
                throw new ArgumentException($"{nameof(updateRequest)} is mandatory");

            if (string.IsNullOrWhiteSpace(updateRequest.UserName))
                throw new ArgumentException("UserName is required.");
        }
    }
}
