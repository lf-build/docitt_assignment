﻿using LendFoundry.Security.Identity;
using Docitt.AssignmentEngine.Services;

using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
using LendFoundry.Security.Tokens;

using System;
using System.Linq;

using LendFoundry.Tenant.Client;
using LendFoundry.Configuration;
using LendFoundry.Security.Identity.Client;
using LendFoundry.EventHub;
using LendFoundry.Foundation.Listener;
using System.Collections.Generic;

namespace Docitt.AssignmentEngine
{
    public class InviteListener : ListenerBase, IInviteListener
    {
        public InviteListener
        (
            IConfigurationServiceFactory configurationFactory,
            ITokenHandler tokenHandler,
            IEventHubClientFactory eventHubFactory,
            ILoggerFactory loggerFactory,
            ITenantServiceFactory tenantServiceFactory,
            IInviteServiceFactory inviteService,
            IIdentityServiceFactory identityWrapper,
            IAssignmentServiceFactory assignmentServiceFactory
        ) : base(tokenHandler, eventHubFactory, loggerFactory, tenantServiceFactory, Settings.ServiceName)
        {
            if (configurationFactory == null) throw new ArgumentException($"{nameof(configurationFactory)} cannot be null");
            if (tokenHandler == null) throw new ArgumentException($"{nameof(tokenHandler)} cannot be null");
            if (eventHubFactory == null) throw new ArgumentException($"{nameof(eventHubFactory)} cannot be null");

            if (loggerFactory == null) throw new ArgumentException($"{nameof(loggerFactory)} cannot be null");
            if (tenantServiceFactory == null) throw new ArgumentException($"{nameof(tenantServiceFactory)} cannot be null");

            if (inviteService == null) throw new ArgumentException($"{nameof(inviteService)} cannot be null");
            if (identityWrapper == null) throw new ArgumentException($"{nameof(identityWrapper)} cannot be null");


            ConfigurationFactory = configurationFactory;
            TokenHandler = tokenHandler;
            EventHubFactory = eventHubFactory;
            LoggerFactory = loggerFactory;
            TenantServiceFactory = tenantServiceFactory;
            _InviteService = inviteService;
            IdentityWrapper = identityWrapper;
            AssignmentServiceFactory = assignmentServiceFactory;
        }

        IIdentityServiceFactory IdentityWrapper { get; }
        private IInviteServiceFactory _InviteService { get; }
        private IAssignmentServiceFactory AssignmentServiceFactory { get; }
        private IConfigurationServiceFactory ConfigurationFactory { get; }

        private IEventHubClientFactory EventHubFactory { get; }

        private ITenantServiceFactory TenantServiceFactory { get; }

        private ITokenHandler TokenHandler { get; }

        private ILoggerFactory LoggerFactory { get; }

        public override List<string> GetEventNames(string tenant)
        {
            var token = TokenHandler.Issue(tenant, Settings.ServiceName);
            var reader = new StaticTokenReader(token.Value);
            var configurationService = ConfigurationFactory.Create<AssignmentServiceConfiguration>(Settings.ServiceName, reader);
            configurationService.ClearCache(Settings.ServiceName);
            var configuration = configurationService.Get();
            if (configuration == null)
            {
                return null;
            }
            else
            {
                return configuration.EventMappings.Select(x => x.EventName).Distinct().ToList();
            }
        }

        public override List<string> SubscribeEvents(string tenant, ILogger logger)
        {
            try
            {
                var token = TokenHandler.Issue(tenant, Settings.ServiceName);
                var reader = new StaticTokenReader(token.Value);
                var eventhub = EventHubFactory.Create(reader);
                var inviteservice = _InviteService.Create(reader, logger, TokenHandler);
                var assignmentService = AssignmentServiceFactory.Create(reader, logger);
                var identityservice = IdentityWrapper.Create(reader);
                var configurationService = ConfigurationFactory.Create<AssignmentServiceConfiguration>(Settings.ServiceName, reader);
                configurationService.ClearCache(Settings.ServiceName);
                var configuration = configurationService.Get();

                if (configuration == null)
                {
                    logger.Error($"The configuration for service #{Settings.ServiceName} could not be found for {tenant} , please verify");
                    return null;
                }
                else
                {
                    var eventMappings = configuration.EventMappings;

                    if (eventMappings == null)
                        throw new Exception("Event Mapping configuration not found ");

                    logger.Info($"#{eventMappings.Count} entity(ies) found from configuration: {Settings.ServiceName}");

                    eventMappings.ForEach(eventName =>
                    {
                        if (eventName.EventName.ToString().ToLower() ==
                            ListenerEvents.UpdateSpouseInvitationDetails.ToString().ToLower())
                        {
                            eventhub.On(eventName.EventName, UpdateSpouseInvitationDetails(logger, configuration, inviteservice, identityservice, assignmentService));
                        }
                        else
                        {
                            eventhub.On(eventName.EventName, CheckInvitation(logger, configuration, inviteservice, identityservice, assignmentService));   
                        }

                        logger.Info($"It was made subscription to EventHub with the Event: #{eventName} for tenant {tenant}");
                    });
                    return eventMappings.Select(x => x.EventName).Distinct().ToList();
                }
            }
            catch (Exception ex)
            {
                logger.Error($"Unable to subscribe event for ${tenant}", ex);
                return new List<string>();
            }
        }

        private static Action<EventInfo> UpdateSpouseInvitationDetails(ILogger logger, AssignmentServiceConfiguration configuration, IInviteService inviteService, LendFoundry.Security.Identity.Client.IIdentityService identityService, IAssignmentService assignmentService)
        {
            return @event =>
            {
                var eventMapping =
                    configuration.EventMappings.FirstOrDefault(e => string.Equals(e.EventName, @event.Name, StringComparison.CurrentCultureIgnoreCase));

                if (eventMapping == null)
                    throw new Exception($"Event Mapping configuration not found for event : {@event.Name}");

                try
                {
                    var inviteId = eventMapping.InviteId.FormatWith(@event);
                    logger.Debug($"Processing UpdateSpouseInvitationDetails for InviteId {inviteId}");     
                    var inviteFirstName = eventMapping.FirstName.FormatWith(@event);
                    var inviteLastName = eventMapping.LastName.FormatWith(@event);
                    if (!string.IsNullOrEmpty(inviteId) && !string.IsNullOrEmpty(inviteFirstName) && !string.IsNullOrEmpty(inviteLastName))
                    {
                        var result = inviteService.UpdateInviteeNameDetails(inviteId, inviteFirstName, inviteLastName).Result;
                        logger.Debug($"FirstName : {inviteFirstName}, LastName : {inviteLastName} are updated for InviteId {inviteId}");
                    }
                }
                catch (Exception ex)
                {
                    logger.Info(" Error : " + ex.Message + " ::::InnerException:" + ex.InnerException);
                }
            };
        }

        private static Action<EventInfo> CheckInvitation(ILogger logger, AssignmentServiceConfiguration configuration, IInviteService inviteService, LendFoundry.Security.Identity.Client.IIdentityService identityService, IAssignmentService assignmentService)
        {
            return @event =>
            {
                var eventMapping =
                  configuration.EventMappings.FirstOrDefault(e => string.Equals(e.EventName, @event.Name, StringComparison.CurrentCultureIgnoreCase));

                if (eventMapping == null)
                    throw new Exception($"Event Mapping configuration not found for event : {@event.Name}");

                //string userName = "{Data.Username}";
                //var user = userName.FormatWith(@event);
                var userName = eventMapping.Username.FormatWith(@event);

                IUserInfo userData = null;
                try
                {
                    userData = identityService.GetUser(userName).Result;

                    if (userData != null)
                    {
                        logger.Info($"User Profile found for #{userName}");
                        if (eventMapping.EventName.ToLower() == ListenerEvents.SpouseCopiedSuccessFully.ToString().ToLower())
                        {
                            // Invitation assignment for copied spouse data after user logged in.
                            var entityId = eventMapping.EntityId.FormatWith(@event);
                            AssignmentForCopiedSpouseData(userName, userData, entityId, inviteService, assignmentService,logger);
                            
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(userData.InvitationReferenceId))
                            {
                                logger.Info($"Invitation Reference Id #{userData.InvitationReferenceId}");
                                var invitationData = inviteService.Get(userData.InvitationReferenceId).Result;
                                if (invitationData != null)
                                {
                                    IInviteUpdateRequest objRequest = new InviteUpdateRequest();
                                    objRequest.UserId = userData.Id;
                                    objRequest.UserName = userName;
                                    objRequest.EmailAddress = userData.Email;
                                    objRequest.InviteRefId = userData.InvitationReferenceId;
                                    var inviteData = inviteService.UpdateInvite(invitationData.EntityType, objRequest).Result;
                                }
                                else
                                {
                                    logger.Info($"Invitation Reference Id #{userData.InvitationReferenceId} not valid.");
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    logger.Info(" Error : " + ex.Message + " ::::InnerException:" + ex.InnerException);
                }

            };
        }

        private static void AssignmentForCopiedSpouseData(string userName,IUserInfo userData, string entityId, IInviteService inviteService, IAssignmentService assignmentService, ILogger logger)
        {
            if (!string.IsNullOrWhiteSpace(userData.Username))
            {
                logger.Info($"User with username - {userData.Username}");
                var invitationData = inviteService.GetInviteByUser(userData.Username).Result;
                if (invitationData != null)
                {

                    var objRequest = new AssignmentRequest()
                    {
                        UserId = userData.Id,
                        User = userName,
                        Role = invitationData.Role,
                        Team = invitationData.Team.ToString()
                    };
                    var assignment = assignmentService.Assign(invitationData.EntityType, entityId, objRequest).Result;
                }
                else
                {
                    logger.Info($"Invitation with username {userData.Username} not valid.");
                }
            }
        }
        private static string UppercaseFirst(string s)
        {
            if (string.IsNullOrEmpty(s)) return string.Empty;
            return $"{char.ToUpper(s[0])}{s.Substring(1)}";
        }
    }
}
