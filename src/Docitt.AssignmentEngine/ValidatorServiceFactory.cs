﻿using Docitt.AssignmentEngine.Services;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Lookup.Client;
using LendFoundry.Security.Tokens;

#if DOTNET2
using Microsoft.Extensions.DependencyInjection;
#else
using Microsoft.Framework.DependencyInjection;
#endif
using System;



namespace Docitt.AssignmentEngine
{
    public class ValidatorServiceFactory : IValidatorServiceFactory
    {
        public ValidatorServiceFactory(IServiceProvider provider)
        {
            if (provider == null) throw new ArgumentException($"{nameof(provider)} cannot be null");

            Provider = provider;
        }

        private IServiceProvider Provider { get; }

        public IValidatorService Create(ITokenReader reader, ILogger logger)
        {
            return new ValidatorService
            (
                tokenReader: Provider.GetService<ITokenReader>(),
                tokenParser: Provider.GetService<ITokenHandler>(),
                lookup: Provider.GetService<ILookupClientFactory>().Create(reader)
            );
        }
    }
}
