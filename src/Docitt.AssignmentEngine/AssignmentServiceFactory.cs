﻿
using LendFoundry.Foundation.Logging;
using LendFoundry.Security.Tokens;
using System;
using LendFoundry.EventHub;
using LendFoundry.Foundation.Date;

#if DOTNET2
using Microsoft.Extensions.DependencyInjection;
#else
using Microsoft.Framework.DependencyInjection;
#endif
using LendFoundry.Foundation.Lookup;
using LendFoundry.Configuration;

namespace Docitt.AssignmentEngine
{
    public class AssignmentServiceFactory : IAssignmentServiceFactory
    {
        public AssignmentServiceFactory(IServiceProvider provider)
        {
            if (provider == null) throw new ArgumentException($"{nameof(provider)} cannot be null");

            Provider = provider;
        }

        private IServiceProvider Provider { get; }

        public IAssignmentService Create(ITokenReader reader, ILogger logger)
        {
            var assignmentRepositoryFactory = Provider.GetService<IAssignmentRepositoryFactory>();
            var assignmentRepository = assignmentRepositoryFactory.Create(reader);

            var tenantTimeFactory = Provider.GetService<ITenantTimeFactory>();
            var configFactory = Provider.GetService<IConfigurationServiceFactory>();
            var tenantTime = tenantTimeFactory.Create(configFactory, reader);

            var eventHubFactory = Provider.GetService<IEventHubClientFactory>();
            var eventHub = eventHubFactory.Create(reader);

            var validatorFactory = Provider.GetService<IValidatorServiceFactory>();
            var validator = validatorFactory.Create(reader, logger);

            return new AssignmentService(assignmentRepository,tenantTime, eventHub, validator);
        }
    }
}
