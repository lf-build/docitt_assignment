﻿using Docitt.AssignmentEngine.Services;
using LendFoundry.Foundation.Lookup;
using LendFoundry.Foundation.Services;
using LendFoundry.Security.Tokens;
using System;
using System.Linq;
using System.Text.RegularExpressions;



namespace Docitt.AssignmentEngine
{
    public class ValidatorService : IValidatorService
    {
        public ValidatorService(ITokenReader tokenReader, ITokenHandler tokenParser, ILookupService lookup)
        {
            if (tokenReader == null)
                throw new ArgumentException($"{nameof(tokenReader)} is mandatory");
            if (tokenParser == null)
                throw new ArgumentException($"{nameof(tokenParser)} is mandatory");
            if (lookup == null)
                throw new ArgumentException($"{nameof(lookup)} is mandatory");
            Lookup = lookup;

            TokenReader = tokenReader;
            TokenParser = tokenParser;
        }
        private ITokenReader TokenReader { get; }
        private ITokenHandler TokenParser { get; }
        private ILookupService Lookup { get; }


        public void EnsureEntityId(string entityId)
        {
            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentException("EntityId is required.");
        }

        public string EnsureEntityType(string entityType)
        {
            if (string.IsNullOrWhiteSpace(entityType))
                throw new InvalidArgumentException("Invalid Entity Type");

            entityType = entityType.ToLower();
            var validEntityTypes = Lookup.GetLookupEntry("entityTypes", entityType);
            if (validEntityTypes == null || !validEntityTypes.Any())
                throw new InvalidArgumentException("Invalid Entity Type");


            return entityType;
        }

        public string EnsureCurrentUser()
        {
            var token = TokenParser.Parse(TokenReader.Read());
            var username = token?.Subject;
            if (string.IsNullOrWhiteSpace(token?.Subject))
                throw new ArgumentException("username is mandatory");
            return username;
        }


        public string EnsureFirstCharacterCapital(string value)
        {
            var a = value.ToCharArray();
            a[0] = char.ToUpper(a[0]);
            return new string(a);
        }

        public string EnsureEmailAddress(string value)
        {
            Regex regex = new Regex(@"^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$");
            Match match = regex.Match(value);
            if (match.Success)
                return value;
            else
                throw new ArgumentException("Email Address is not in correct format");
        }
    }
}
