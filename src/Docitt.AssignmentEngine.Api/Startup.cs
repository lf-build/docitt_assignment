﻿using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Client;
using LendFoundry.Security.Tokens;
using LendFoundry.Tenant.Client;


using LendFoundry.Foundation.Lookup.Client;
using LendFoundry.Foundation.Persistence.Mongo;
using Docitt.AssignmentEngine.Persistence;
using Docitt.AssignmentEngine.Services;
using LendFoundry.Configuration;
using LendFoundry.Configuration.Client;
using LendFoundry.Email.Client;
using LendFoundry.Security.Identity.Client;
using Docitt.UserProfile.Client;

using System;
using System.Threading.Tasks;
using Jil;
using LendFoundry.Foundation.Services;
using System.Collections.Generic;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.PlatformAbstractions;
using Swashbuckle.AspNetCore.Swagger;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.AspNetCore.Http;
using System.IO;
using LendFoundry.Foundation.ServiceDependencyResolver;
using Docitt.TwilioOTP.Client;
using LendFoundry.TemplateManager.Client;

namespace Docitt.AssignmentEngine.Api
{
    internal class Startup
    {
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("docs", new Info
                {
                    Version = PlatformServices.Default.Application.ApplicationVersion,
                    Title = "DocittAssignmentEngine"
                });
                c.AddSecurityDefinition("Bearer", new ApiKeyScheme()
               {
                   Type = "apiKey",
                   Name = "Authorization",
                   Description = "For accessing the API a valid JWT token must be passed in all the queries in the 'Authorization' header. The syntax used in the 'Authorization' header should be Bearer xxxxx.yyyyyy.zzzz",
                   In = "header"
               });
               c.AddSecurityRequirement(new Dictionary<string, IEnumerable<string>>
               {
                   { "Bearer", new string[] { } }
               });
                c.DescribeAllEnumsAsStrings();
                c.IgnoreObsoleteProperties();
                c.DescribeStringEnumsInCamelCase();
                c.IgnoreObsoleteActions();
                var basePath = PlatformServices.Default.Application.ApplicationBasePath;
                var xmlPath = Path.Combine(basePath, "Docitt.AssignmentEngine.Api.xml");
                c.IncludeXmlComments(xmlPath);
            });
            services.TryAddSingleton<IHttpContextAccessor, HttpContextAccessor>();

            services.AddTenantTime();
            services.AddTokenHandler();
            services.AddHttpServiceLogging(Settings.ServiceName);
            services.AddEventHub(Settings.ServiceName);
            services.AddTenantService();
            services.AddEmailService();
            services.AddConfigurationService<AssignmentServiceConfiguration>(Settings.ServiceName);
            services.AddDependencyServiceUriResolver<AssignmentServiceConfiguration>(Settings.ServiceName);
            services.AddIdentityService();
         
            services.AddTransient(provider => provider.GetRequiredService<IConfigurationService<AssignmentServiceConfiguration>>().Get());
            services.AddUserProfileService();
            services.AddLookupService();
            services.AddTextMessageService();
            services.AddTemplateManagerService();
            // internals
            services.AddMongoConfiguration(Settings.ServiceName);
            
            services.AddTransient<IAssignmentRepository, AssignmentRepository>();
            services.AddTransient<IAssignmentRepositoryFactory, AssignmentRepositoryFactory>();
            services.AddTransient<IAssignmentService, AssignmentService>();
            services.AddTransient<IInviteRepository, InviteRepository>();
            services.AddTransient<IInviteRepositoryFactory, InviteRepositoryFactory>();
            services.AddTransient<IInviteService, InviteService>();
            services.AddTransient<IValidatorService, ValidatorService>();
            services.AddTransient<IInviteListener, InviteListener>();
            services.AddTransient<IInviteServiceFactory, InviteServiceFactory>();
            services.AddTransient<IAssignmentServiceFactory, AssignmentServiceFactory>();
            services.AddTransient<IValidatorServiceFactory, ValidatorServiceFactory>();
            services.AddTransient<IEnsureHelper, EnsureHelper>();
            services.AddMvc().AddLendFoundryJsonOptions();
            services.AddCors();
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseHealthCheck();
		    app.UseCors(env);
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/docs/swagger.json", "DOCITT Assignment Service");
            });
            app.UseErrorHandling();
                       
            app.UseRequestLogging();
            app.UseInviteListener();
            app.UseMvc();
            app.UseConfigurationCacheDependency();
        }
    }
}