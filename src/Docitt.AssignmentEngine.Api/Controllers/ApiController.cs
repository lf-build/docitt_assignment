﻿using Docitt.AssignmentEngine.Services;
using LendFoundry.Foundation.Services;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using Docitt.AssignmentEngine.Events;

namespace Docitt.AssignmentEngine.Api.Controllers
{
    /// <summary>
    /// Represents Api controller class.
    /// </summary>
    public class ApiController : ExtendedController
    {
        /// <summary>
        /// Represents constructor class.
        /// </summary>
        /// <param name="assignmentService"></param>
        /// <param name="inviteService"></param>
        public ApiController(IAssignmentService assignmentService, IInviteService inviteService)
        {
            if (assignmentService == null)
                throw new ArgumentException($"{nameof(assignmentService)} is mandatory");

            if (inviteService == null)
                throw new ArgumentException($"{nameof(inviteService)} is mandatory");


            AssignmentService = assignmentService;
            InviteService = inviteService;
        }

        private IAssignmentService AssignmentService { get; }

        /// <summary>
        /// Get Invite service.
        /// </summary>
        public IInviteService InviteService { get; }
        private static NoContentResult NoContentResult { get; } = new NoContentResult();

        /// <summary>
        /// Assigned me with specific entityType and entityId.
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <returns></returns>
        [HttpGet("{entityType}/{entityId}/assigned/me")]
        [ProducesResponseType(typeof(IAssignedResponse), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public Task<IActionResult> IsAssignedToMe(string entityType, string entityId)
        {
            return ExecuteAsync(async () => Ok(await AssignmentService.IsAssigned(entityType, entityId)));
        }

        /// <summary>
        /// Get all assignments with entity type and entityid.
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <returns></returns>
        [HttpGet("{entityType}/{entityId}/assignments")]
        [ProducesResponseType(typeof(IEnumerable<IAssignment>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public Task<IActionResult> GetAllAssignments(string entityType, string entityId)
        {
            return ExecuteAsync(async () => Ok(await AssignmentService.Get(entityType, entityId)));
        }

        /// <summary>
        /// Get assignment by role with entity type,entityid and role.
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="role"></param>
        /// <returns></returns>
        [HttpGet("{entityType}/{entityId}/assignment/{role}")]
        [ProducesResponseType(typeof(IAssignment), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> GetAssignmentByRole(string entityType, string entityId, string role)
        {
            try
            {
                if (!string.IsNullOrWhiteSpace(role))
                    role = WebUtility.UrlDecode(role);

                var res = await AssignmentService.GetByRole(entityType, entityId, role);
                return Ok(res);
            }
            catch (ArgumentException exception)
            {

                return ErrorResult.BadRequest(exception.Message);
            }
            catch (Exception ex)
            {
                return ErrorResult.BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Get loan officer details by invitation token or branded site unique code on welcome video page
        /// </summary>
        /// <param name="key">key (refid/reference)</param>
        /// <param name="value">value (inviationtoken/ brandedsiteuniquecode</param>
        /// <returns></returns>
        [HttpGet("key/{key}/value/{value}")]
        [ProducesResponseType(typeof(IInviteVM), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public Task<IActionResult> GetInvitedByLoanOfficer(string key, string value)
        {
            // value is invitationToken or BrandedSiteUniqueCode
            if (!string.IsNullOrWhiteSpace(value))
                value = WebUtility.UrlDecode(value);

            return ExecuteAsync(async () => Ok(await InviteService.GetInviteData(key, value)));
        }


        /// <summary>
        /// Get all by user with entity type and username.
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="username"></param>
        /// <returns></returns>
        [HttpGet("{entityType}/assignments/{username}")]
        [ProducesResponseType(typeof(IEnumerable<IAssignment>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public Task<IActionResult> GetAllByUser(string entityType, string username)
        {
            return ExecuteAsync(async () => Ok(await AssignmentService.GetByUser(entityType, username)));
        }

        /// <summary>
        /// Assign with entity type and entity id.
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="assignmentRequest"></param>
        /// <returns></returns>
        [HttpPost("{entityType}/{entityId}/assign")]
        [ProducesResponseType(typeof(IAssignment), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public Task<IActionResult> Assign(string entityType, string entityId, [FromBody]AssignmentRequest assignmentRequest)
        {
            return ExecuteAsync(async () => Ok(await AssignmentService.Assign(entityType, entityId, assignmentRequest)));
        }

        /// <summary>
        /// Multi assign with entity type and entity id.
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="assignmentRequest"></param>
        /// <returns></returns>
        [HttpPost("{entityType}/{entityId}/multiassign")]
        [ProducesResponseType(typeof(IEnumerable<IAssignment>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public Task<IActionResult> MultiAssign(string entityType, string entityId, [FromBody]List<AssignmentRequest> assignmentRequest)
        {
            return ExecuteAsync(async () => Ok(await AssignmentService.MultiAssign(entityType, entityId, assignmentRequest)));
        }

        /// <summary>
        /// Get associated user by specific role.
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="role"></param>
        /// <returns></returns>
        [HttpGet("{userName}/{role}")]
        [ProducesResponseType(typeof(string), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public Task<IActionResult> GetAssociatedUserByRole(string userName, string role)
        {
            if (!string.IsNullOrWhiteSpace(role))
                role = WebUtility.UrlDecode(role);

            return ExecuteAsync(async () => Ok(await InviteService.GetAssociatedUserInfoByRole(userName, role)));
        }


        /// <summary>
        /// Assign with entity type,entityid and username
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="userName"></param>
        /// <returns></returns>
        [HttpPut("{entityType}/{entityId}/{userName}/assignment")]
        [ProducesResponseType(typeof(IAssignmentResponse), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public Task<IActionResult> Assign(string entityType, string entityId, string userName)
        {
            return ExecuteAsync(async () => Ok(await InviteService.Assign(entityType, entityId, userName)));
        }

        /// <summary>
        ///  Send invite with specific entity type, entity id and email.
        /// </summary>
        /// <param name="entityType">entity type</param>
        /// <param name="entityId">Usually Invitee email address or username</param>
        /// <param name="inviteRequest">Inviterequest modal</param>
        /// <param name="allowEmail">bool flag to indicate email to be send from docitt</param>
        /// <returns></returns>
        [HttpPost("{entityType}/{entityId}/invite/send/{allowEmail?}")]
        [ProducesResponseType(typeof(IInvite), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public Task<IActionResult> SendInvite(string entityType, string entityId, [FromBody]InviteRequest inviteRequest, bool allowEmail = true)
        {
            return ExecuteAsync(async () => Ok(await InviteService.SendInvite(entityType, entityId, inviteRequest, allowEmail)));
        }

        /// <summary>
        /// Send Invitation Reminder
        /// </summary>
        /// <param name="invitationId">Invitation Id</param>
        /// <param name="sendEmail">To send reminder mail or not , true means email will be sent other wise not</param>
        /// <param name="entityId">entity Id that has to value while copy spouse data</param>
        /// <returns>Invitation details</returns>
        [HttpPut("invite-lead/{invitationId}/send-reminder/{sendEmail?}/{entityId?}")]
        [ProducesResponseType(typeof(IInvite), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public Task<IActionResult> SendInviteReminder(string invitationId, bool sendEmail = true, string entityId ="")
        {
            return ExecuteAsync(async () => Ok(await InviteService.SendReminder(invitationId, sendEmail, entityId)));
        }
        
        /// <summary>
        /// Send Reminder for continuing on application
        /// </summary>
        /// <param name="entityType">application</param>
        /// <param name="entityId">application number</param>
        /// <param name="templateName">template name</param>
        /// <param name="payload">email payload</param>
        /// <returns></returns>
        [HttpPost("applicationremindernotification/{entityType}/{entityId}/{templateName}")]
        [ProducesResponseType(typeof(bool), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public Task<IActionResult> ApplicationReminderNotification(string entityType, string entityId, string templateName, [FromBody] ApplicationReminderNotification payload)
        {
            return ExecuteAsync(async () => Ok(await InviteService.ApplicationReminderNotification(entityType, entityId, templateName, payload)));
        }

        /// <summary>
        ///  Get invite with invitereferenceid.
        /// </summary>
        /// <param name="invitereferenceid"></param>
        /// <returns></returns>
        [HttpGet("/invite/{invitereferenceid}")]
        [ProducesResponseType(typeof(IInvite), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public Task<IActionResult> GetInvite(string invitereferenceid)
        {
            return ExecuteAsync(async () => Ok(await InviteService.Get(invitereferenceid)));
        }

        /// <summary>
        /// GetInviteInformation
        /// </summary>
        /// <param name="userName"></param>
        /// <returns></returns>
        [HttpGet("/invite/{userName}/info")]
        [ProducesResponseType(typeof(IInvite), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public Task<IActionResult> GetInviteInformation(string userName)
        {
            return ExecuteAsync(async () => Ok(await InviteService.GetInviteByUser(userName)));
        }


        /// <summary>
        /// Get All Lead invite
        /// </summary>
        /// <returns></returns>
        [HttpGet("/invite-lead/all")]
        [ProducesResponseType(typeof(IInvite), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public Task<IActionResult> GetAllLeadInvite()
        {
            return ExecuteAsync(async () => Ok(await InviteService.GetAllLeadInvite()));
        }

        /// <summary>
        /// Get All Lead invite that is invited by supplied user email.
        /// </summary>
        /// <returns></returns>
        [HttpGet("/invite-lead/{invitedByEmail}")]
        [ProducesResponseType(typeof(IInvite), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public Task<IActionResult> GetAllLeadInvitedBy(string invitedByEmail)
        {
            return ExecuteAsync(async () => Ok(await InviteService.GetAllLeadInvitedBy(invitedByEmail)));
        }

        /// <summary>
        /// Get associated user by specific role for supplied invitation reference.
        /// </summary>
        /// <param name="invitationReferenceId">Invitation reference id</param>
        /// <param name="role">Role to be find in association</param>
        /// <returns>associated user name</returns>
        [HttpGet("/invite-lead/{invitationReferenceId}/{role}")]
        [ProducesResponseType(typeof(string), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public Task<IActionResult> GetAssociatedUserByRoleForLead(string invitationReferenceId, string role)
        {
            if (!string.IsNullOrWhiteSpace(role))
                role = WebUtility.UrlDecode(role);

            return ExecuteAsync(async () => Ok(await InviteService.GetAssociatedUserInfoByRoleForLead(invitationReferenceId, role)));
        }

        /// <summary>
        /// Get Lead invite information for supplied user email.
        /// </summary>
        /// <returns></returns>
        [HttpGet("/invite-lead/{inviteeEmail}/info")]
        [ProducesResponseType(typeof(IInvite), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public Task<IActionResult> GetLeadInviteInformationByInvitee(string inviteeEmail)
        {
            return ExecuteAsync(async () => Ok(await InviteService.GetLeadInviteInformationByInvitee(inviteeEmail)));
        }



        /// <summary>
        ///  Update invite with entity type.
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="updateRequest"></param>
        /// <returns></returns>
        [HttpPost("{entityType}/invite/update")]

        [ProducesResponseType(typeof(IInvite), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]

        public Task<IActionResult> UpdateInvite(string entityType, [FromBody]InviteUpdateRequest updateRequest)
        {
            return ExecuteAsync(async () => Ok(await InviteService.UpdateInvite(entityType, updateRequest)));
        }

        /// <summary>
        /// Verify invite with entity type.
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="inviteVerifyRequest"></param>
        /// <returns></returns>
        [HttpPost("{entityType}/invite/verify")]
        [ProducesResponseType(typeof(IInvite), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public Task<IActionResult> verify(string entityType, [FromBody]InviteVerifyRequest inviteVerifyRequest)
        {
            return ExecuteAsync(async () => Ok(await InviteService.VerifyToken(entityType, inviteVerifyRequest)));
        }

        /// <summary>
        /// Unassign with entity type,entity id and username.
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="username"></param>
        /// <returns></returns>
        [HttpDelete("{entityType}/{entityId}/disabled/{username}")]
        [ProducesResponseType(204)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public Task<IActionResult> Unassign(string entityType, string entityId, string username)
        {         
            return ExecuteAsync(async () =>
            {
                await AssignmentService.Unassign(entityType, entityId, username);
                return NoContentResult;
            });
        }

        /// <summary>
        /// Release with entity type, entity id and username.
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="username"></param>
        /// <returns></returns>
        [HttpPost("{entityType}/{entityId}/release/{username}")]
        [ProducesResponseType(204)] 
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public Task<IActionResult> Release(string entityType, string entityId, string username)
        {          
            return ExecuteAsync(async () =>
            {
                await AssignmentService.Release(entityType, entityId, username);
                return NoContentResult;
            });
        }

        /// <summary>
        ///  Expire the InvitationTokenExpiry.
        /// </summary>
        /// <param name="invitationId"></param>
        /// <returns>bool</returns>
        [HttpPost("expireinvitation/{invitationId}")]
        [ProducesResponseType(typeof(IInvite), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public Task<IActionResult> ExpireInvitation(string invitationId)
        {
            return ExecuteAsync(async () => Ok(await InviteService.ExpireInvitation(invitationId)));
        }

        /// <summary>
        /// Get All Lead invite that is invited by supplied user email and role.
        /// </summary>
        /// <returns></returns>
        [HttpGet("{entityType}/{entityId}/invite-lead-info/{invitedByEmail}/{role}")]
        [ProducesResponseType(typeof(IInvite), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public Task<IActionResult> GetInviteInfoByEmailAndRole(string entityType, string entityId, string invitedByEmail, string role)
        {
            return ExecuteAsync(async () => Ok(await InviteService.GetInviteInfoByEmailAndRole(entityType, entityId, invitedByEmail, role)));
        }


        /// <summary>
        ///  Expire the Invitation for multiple invite.
        /// </summary>
        /// <param name="invitationIds"> List of invitation id to be expired</param>
        /// <returns>bool</returns>
        [HttpPut("expire-invitations")]
        [ProducesResponseType(typeof(bool), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public Task<IActionResult> ExpireInvitation([FromBody]string[] invitationIds)
        {
            return ExecuteAsync(async () => Ok(await InviteService.ExpireInvitations(invitationIds)));
        }

        /// <summary>
        /// Regenerate Invite
        /// </summary>
        /// <param name="invitationId"> Invitation Id </param>
        /// <param name="inviteEmailId"> Invite EmailId </param>
        /// <returns>Invite model</returns>
        [HttpPut("regenerateinvitation/{invitationId}/{inviteEmailId}")]
        [ProducesResponseType(typeof(IInvite), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public Task<IActionResult> RegenerateInviteToken(string invitationId, string inviteEmailId)
        {
            return ExecuteAsync(async () => Ok(await InviteService.RegenerateInviteToken(invitationId, inviteEmailId)));
        }

        /// <summary>
        /// Get Invitation information using id.
        /// </summary>
        /// <param name="invitationId">Inviteation Id</param>
        /// <returns>Invite response</returns>
       [HttpGet("/invite-by-id/{invitationId}")]
        [ProducesResponseType(typeof(IInvite), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public Task<IActionResult> GetInviteById(string invitationId)
        {
            return ExecuteAsync(async () => Ok(await InviteService.GetInviteById(invitationId)));
        }
        
    }
}
