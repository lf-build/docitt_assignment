﻿using System.Collections.Generic;
using LendFoundry.Foundation.Services;
using Moq;
using RestSharp;
using Xunit;
using LendFoundry.Foundation.Client;

namespace Docitt.AssignmentEngine.Client.Tests
{
    public class AssignmentServiceClientTests
    {
        private Mock<IServiceClient> ServiceClient { get; }
        private IAssignmentService Client { get; }
        private IRestRequest Request { get; set; }

        public AssignmentServiceClientTests()
        {
            ServiceClient = new Mock<IServiceClient>();
            Client = new AssignmentService(ServiceClient.Object);
        }

        [Fact]
        public async void Client_IsAssigned()
        {
            ServiceClient.Setup(s => s.ExecuteAsync<AssignedResponse>(It.IsAny<IRestRequest>()))
                .Callback<IRestRequest>(r => Request = r)
                .ReturnsAsync(new AssignedResponse());

            var result = await Client.IsAssigned("application", "000001");

            ServiceClient.Verify(x => x.ExecuteAsync<AssignedResponse>(It.IsAny<IRestRequest>()), Times.Once);

            Assert.Equal("/{entityType}/{entityId}/assigned/me", Request.Resource);
            Assert.Equal(Method.GET, Request.Method);
            Assert.NotNull(result);
        }

        [Fact]
        public async void Client_Get()
        {
            ServiceClient.Setup(s => s.ExecuteAsync<List<Assignment>>(It.IsAny<IRestRequest>()))
                .Callback<IRestRequest>(r => Request = r)
                .ReturnsAsync(new List<Assignment>());

            var result = await Client.Get("application", "000001");

            ServiceClient.Verify(x => x.ExecuteAsync<List<Assignment>>(It.IsAny<IRestRequest>()), Times.Once);

            Assert.Equal("/{entityType}/{entityId}/assignments", Request.Resource);
            Assert.Equal(Method.GET, Request.Method);
            Assert.NotNull(result);
        }

        [Fact]
        public async void Client_GetByUser()
        {
            ServiceClient.Setup(s => s.ExecuteAsync<List<Assignment>>(It.IsAny<IRestRequest>()))
                .Callback<IRestRequest>(r => Request = r)
                .ReturnsAsync(new List<Assignment>());

            var result = await Client.GetByUser("application", "jondoe");

            ServiceClient.Verify(x => x.ExecuteAsync<List<Assignment>>(It.IsAny<IRestRequest>()), Times.Once);

            Assert.Equal("/{entityType}/assignments/{username}", Request.Resource);
            Assert.Equal(Method.GET, Request.Method);
            Assert.NotNull(result);
        }

      
        [Fact]
        public async void Client_Assign()
        {
            ServiceClient.Setup(s => s.ExecuteAsync<Assignment>(It.IsAny<IRestRequest>()))
                .Callback<IRestRequest>(r => Request = r)
                .ReturnsAsync(new Assignment());

            var result = await Client.Assign("application", "000001", new AssignmentRequest());

            ServiceClient.Verify(x => x.ExecuteAsync<Assignment>(It.IsAny<IRestRequest>()), Times.Once);

            Assert.Equal("/{entityType}/{entityId}/assign", Request.Resource);
            Assert.Equal(Method.POST, Request.Method);
            Assert.NotNull(result);
        }

        [Fact]
        public async void Client_MultiAssign()
        {
            ServiceClient.Setup(s => s.ExecuteAsync<List<Assignment>>(It.IsAny<IRestRequest>()))
                .Callback<IRestRequest>(r => Request = r)
                .ReturnsAsync(new List<Assignment>());

            var result = await Client.MultiAssign("application", "000001", new List<AssignmentRequest>());

            ServiceClient.Verify(x => x.ExecuteAsync<List<Assignment>>(It.IsAny<IRestRequest>()), Times.Once);

            Assert.Equal("/{entityType}/{entityId}/multiassign", Request.Resource);
            Assert.Equal(Method.POST, Request.Method);
            Assert.NotNull(result);
        }

        [Fact]
        public async void Client_Unassign()
        {
            ServiceClient.Setup(s => s.ExecuteAsync(It.IsAny<IRestRequest>()))
                .Callback<IRestRequest>(r => Request = r)
                .ReturnsAsync(true);

            await Client.Unassign("application", "000001", "VP of Sales");

            ServiceClient.Verify(x => x.ExecuteAsync(It.IsAny<IRestRequest>()), Times.Once);

            Assert.Equal("/{entityType}/{entityId}/disabled/{username}", Request.Resource);
            Assert.Equal(Method.DELETE, Request.Method);
        }

        [Fact]
        public async void Client_Release()
        {
            ServiceClient.Setup(s => s.ExecuteAsync(It.IsAny<IRestRequest>()))
                .Callback<IRestRequest>(r => Request = r)
                .ReturnsAsync(true);

            await Client.Release("application", "000001", "VP of Sales");

            ServiceClient.Verify(x => x.ExecuteAsync(It.IsAny<IRestRequest>()), Times.Once);

            Assert.Equal("/{entityType}/{entityId}/release/{username}", Request.Resource);
            Assert.Equal(Method.POST, Request.Method);
        }

       
    }
}
