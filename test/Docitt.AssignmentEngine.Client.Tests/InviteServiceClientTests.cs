﻿using Docitt.AssignmentEngine.Services;
using LendFoundry.Foundation.Client;
using Moq;
using RestSharp;
using Xunit;

namespace Docitt.AssignmentEngine.Client.Tests
{
    public class InviteServiceClientTests
    {
        private Mock<IServiceClient> ServiceClient { get; }
        private IInviteService Client { get; }
        private IRestRequest Request { get; set; }

        public InviteServiceClientTests()
        {
            ServiceClient = new Mock<IServiceClient>();
            Client = new InviteService(ServiceClient.Object);
        }

        [Fact]
        public async void Client_SendInvite()
        {
            ServiceClient.Setup(s => s.ExecuteAsync<Invite>(It.IsAny<IRestRequest>()))
                .Callback<IRestRequest>(r => Request = r)
                .ReturnsAsync(new Invite());

            var result = await Client.SendInvite("application", "000001", new InviteRequest(), false);

            ServiceClient.Verify(x => x.ExecuteAsync<Invite>(It.IsAny<IRestRequest>()), Times.Once);

            Assert.Equal("/{entityType}/{entityId}/invite/send", Request.Resource);
            Assert.Equal(Method.POST, Request.Method);
            Assert.NotNull(result);
        }

        [Fact]
        public async void Client_GetInvite()
        {
            ServiceClient.Setup(s => s.ExecuteAsync<Invite>(It.IsAny<IRestRequest>()))
                .Callback<IRestRequest>(r => Request = r)
                .ReturnsAsync(new Invite());

            var result = await Client.Get("1234567");

            ServiceClient.Verify(x => x.ExecuteAsync<Invite>(It.IsAny<IRestRequest>()), Times.Once);

            Assert.Equal("/invite/{invitereferenceid}", Request.Resource);
            Assert.Equal(Method.GET, Request.Method);
            Assert.NotNull(result);
        }



        [Fact]
        public async void Client_VerifyToken()
        {
            ServiceClient.Setup(s => s.ExecuteAsync<Invite>(It.IsAny<IRestRequest>()))
               .Callback<IRestRequest>(r => Request = r)
               .ReturnsAsync(new Invite());

            var objRequest = new InviteVerifyRequest()
            {
                InviteEmail = "suresh.b@sigmainfo.net",
                InviteRefId = "1234456"
            };

            var result = await Client.VerifyToken("application", objRequest);

            ServiceClient.Verify(x => x.ExecuteAsync<Invite>(It.IsAny<IRestRequest>()), Times.Once);

            Assert.Equal("/{entityType}/invite/verify", Request.Resource);
            Assert.Equal(Method.POST, Request.Method);
            Assert.NotNull(result);

        }


        [Fact]
        public async void Client_UpdateInvite()
        {
            ServiceClient.Setup(s => s.ExecuteAsync<Invite>(It.IsAny<IRestRequest>()))
                .Callback<IRestRequest>(r => Request = r)
                .ReturnsAsync(new Invite());

            var result = await Client.UpdateInvite("application", new InviteUpdateRequest());

            ServiceClient.Verify(x => x.ExecuteAsync<Invite>(It.IsAny<IRestRequest>()), Times.Once);

            Assert.Equal("/{entityType}/invite/update", Request.Resource);
            Assert.Equal(Method.POST, Request.Method);
            Assert.NotNull(result);
        }
    }
}
