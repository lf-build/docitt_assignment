﻿using Docitt.AssignmentEngine.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Docitt.AssignmentEngine.Mocks
{
    public class FakeInviteRepository : IInviteRepository
    {
        public List<IInvite> InviteInMemoryData { get; set; } = new List<IInvite>();
        public FakeInviteRepository()
        {
        }

        public void Add(IInvite item)
        {
            item.Id = Guid.NewGuid().ToString("N");
            InviteInMemoryData.Add(item);
        }

        public void AddWithId(IInvite item)
        {   
            InviteInMemoryData.Add(item);
        }

        public Task<IEnumerable<IInvite>> All(Expression<Func<IInvite, bool>> query, int? skip = default(int?), int? quantity = default(int?))
        {
            return Task.Run(() =>
            {
                return InviteInMemoryData
                    .AsQueryable()
                    .Where(query)
                    .AsEnumerable();
            });
        }

        public int Count(Expression<Func<IInvite, bool>> query)
        {
            return InviteInMemoryData
                .AsQueryable()
                .Count(query);
        }

       
        public Task<IInvite> GetInvite(string entityType, string inviteRefId)
        {
            return Task.FromResult<IInvite>(InviteInMemoryData.Where(p => p.EntityType == entityType && p.InvitationToken== inviteRefId).FirstOrDefault()); 
        }

       
        public Task<IInvite> GetInvite(string entityType, string entityId, string inviteEmail)
        {            
           return Task.FromResult<IInvite>(InviteInMemoryData.Where(p => p.EntityType == entityType && p.EntityId == entityId && p.InviteEmail == inviteEmail).FirstOrDefault());
        }

        public Task<IInvite> Get(string id)
        {
            return Task.FromResult<IInvite>(InviteInMemoryData.Where(p => p.Id == id).FirstOrDefault());
        }

        public void Remove(IInvite item)
        {
            InviteInMemoryData.Remove(item);
        }

          public    void Update(IInvite item)
        {
            var data = InviteInMemoryData.Where(p => p.EntityType == item.EntityType && p.Id == item.Id).FirstOrDefault();
        }

        public Task<IInvite> GetInvite(string inviteReferenceId)
        {
            return Task.FromResult<IInvite>(InviteInMemoryData.Where(p => p.InvitationToken == inviteReferenceId).FirstOrDefault());
        }

        /// <summary>
        /// GetInviteByUserName
        /// </summary>
        /// <param name="userName"></param>
        /// <returns></returns>
        public Task<IInvite> GetInviteByUser(string userName)
        {
            return Task.FromResult<IInvite>(InviteInMemoryData.Where(p => p.UserName == userName).FirstOrDefault());
        }

        public Task<IEnumerable<IInvite>> GetAllLeadInvite()
        {
            return Task.FromResult<IEnumerable<IInvite>>(InviteInMemoryData.Where(p => p.UserName == null || p.UserName == "").ToList());
        }

        public Task<IEnumerable<IInvite>> GetAllLeadInvitedBy(string invitedByEmail)
        {
            return Task.FromResult<IEnumerable<IInvite>>(InviteInMemoryData.Where(p => (p.UserName == null || p.UserName == "") && p.InvitedBy == invitedByEmail).ToList());
        }
        public Task<IInvite> GetLeadInviteInformationByInvitee(string inviteeEmail)
        {
            return Task.FromResult<IInvite>(InviteInMemoryData.FirstOrDefault(p => p.InviteEmail == inviteeEmail));
        }

        public Task<IEnumerable<IInvite>> GetAllLeadInvite(string inviteLeadRole)
        {
            return Task.FromResult<IEnumerable<IInvite>>(InviteInMemoryData.Where(p => (p.UserName == null || p.UserName == "") && p.Role.ToLower() == inviteLeadRole.ToLower()).ToList());
        }

        public Task<IEnumerable<IInvite>> GetAllLeadInvitedBy(string invitedByEmail, string inviteLeadRole)
        {
            return Task.FromResult<IEnumerable<IInvite>>(InviteInMemoryData.Where(p => (p.UserName == null || p.UserName == "") && p.InvitedBy == invitedByEmail && p.Role.ToLower() == inviteLeadRole.ToLower()).ToList());
        }
       
        public Task<IInvite> GetInviteById(string invitationId)
        {
            throw new NotImplementedException();
        }
    }
}
