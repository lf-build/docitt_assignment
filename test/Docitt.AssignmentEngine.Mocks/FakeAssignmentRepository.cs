﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Docitt.AssignmentEngine.Mocks
{
    public class FakeAssignmentRepository :IAssignmentRepository
    {
        public List<IAssignment> AssignmentInMemoryData { get; set; } = new List<IAssignment>();
        public FakeAssignmentRepository()
        {
        }

        public void Add(IAssignment item)
        {
            item.Id = Guid.NewGuid().ToString("N");
            AssignmentInMemoryData.Add(item);
        }

        public Task<IEnumerable<IAssignment>> All(Expression<Func<IAssignment, bool>> query, int? skip = default(int?), int? quantity = default(int?))
        {
            return Task.Run(() =>
            {
                return AssignmentInMemoryData
                    .AsQueryable()
                    .Where(query)
                    .AsEnumerable();
            });
        }

        public int Count(Expression<Func<IAssignment, bool>> query)
        {
            return AssignmentInMemoryData
                .AsQueryable()
                .Count(query);
        }

        public Task<IAssignment> Get(string id)
        {
            return Task.FromResult<IAssignment>(AssignmentInMemoryData.Where(p => p.Id == id).FirstOrDefault());
        }

        public Task<IEnumerable<IAssignment>> Get(string entityType, string entityId)
        {
            return Task.FromResult<IEnumerable<IAssignment>>(AssignmentInMemoryData.Where(p => p.EntityType == entityType && p.EntityId == entityId));
        }

     
        public Task<IAssignment> GetByRole(string entityType, string entityId, string role)
        {
            return Task.FromResult<IAssignment>(AssignmentInMemoryData.Where(p => p.EntityType == entityType && p.EntityId == entityId && p.Role == role).FirstOrDefault());
        }

        public Task<IEnumerable<IAssignment>> GetByUserName(string entityType, string username)
        {
            return Task.FromResult<IEnumerable<IAssignment>>(AssignmentInMemoryData.Where(p => p.EntityType == entityType && p.Assignee == username));
        }

        public Task<IAssignment> GetByUserName(string entityType, string entityId, string username)
        {
            return Task.FromResult<IAssignment>(AssignmentInMemoryData.Where(p => p.EntityType == entityType && p.EntityId == entityId && p.Assignee == username).FirstOrDefault());
        }

        public void Remove(IAssignment item)
        {
            AssignmentInMemoryData = AssignmentInMemoryData.Where(x => x.Id != item.Id).ToList();
        }

        public void Update(IAssignment item)
        {
           var assignment = AssignmentInMemoryData.Where(x => x.Id == item.Id).FirstOrDefault();
        }
    }
}
