﻿using Moq;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.TemplateManager;
using Docitt.AssignmentEngine.Services;

namespace Docitt.AssignmentEngine.Mocks
{
    public class InMemoryObjects
    {
        protected string TenantId { get; } = "my-tenant";

        

        protected IAssignmentRepository FakeAssignmentRepository { get; set; }

        protected Mock<IEventHubClient> EventHubClient { get; set; } = new Mock<IEventHubClient>();

        protected Mock<ITenantTime> TenantTime { get; set; } = new Mock<ITenantTime>();

        protected Mock<Services.IValidatorService> ValidatorService { get; set; } = new Mock<IValidatorService>();
      
        protected Mock< ITemplateManagerService> TemplateManagerService { get; set; } = new Mock<ITemplateManagerService>();

       
        protected Mock<AssignmentServiceConfiguration> AssignmentServiceConfiguration { get; set; } = new Mock<AssignmentServiceConfiguration>();
        
    }
    public interface IServiceProvider : System.IServiceProvider
    {
        T GetService<T>() where T : class;

        T GetRequiredService<T>() where T : class;
    }
}
