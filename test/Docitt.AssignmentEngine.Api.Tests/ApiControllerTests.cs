﻿using System;
using System.Collections.Generic;
using Docitt.AssignmentEngine.Api.Controllers;
using LendFoundry.Foundation.Services;
using Microsoft.AspNet.Mvc;
using Moq;
using Xunit;
using Docitt.AssignmentEngine.Services;

namespace Docitt.AssignmentEngine.Api.Tests
{
    public class ApiControllerTests
    {
        private Mock<IAssignmentService> Assignment { get; }

        private Mock<IInviteService> InviteService { get; }
        //private Mock<IEscalationService> Escalation { get; } 
        private ApiController Api { get; }

        public ApiControllerTests()
        {
            Assignment = new Mock<IAssignmentService>();
            InviteService = new Mock<IInviteService>();
            //Escalation = new Mock<IEscalationService>();
            Api = new ApiController(Assignment.Object, InviteService.Object);
        }

        [Fact]
        public void Controller_With_Null_Services_Should_Throw_ArgumentException()
        {
            Assert.Throws<ArgumentException>(() => new ApiController(null,null));
        }

        [Fact]
        public async void Controller_IsAssignedToMe_Returns_Result_OnSuccess()
        {
            Assignment.Setup(x => x.IsAssigned(It.IsAny<string>(), It.IsAny<string>()))
                .ReturnsAsync(new AssignedResponse {IsAssigned = true});

            var result = (HttpOkObjectResult) await Api.IsAssignedToMe("application", "000001");
            
            Assert.NotNull(result);
            Assert.Equal(200, result.StatusCode);
            Assert.IsType<AssignedResponse>(result.Value);
        }

        [Fact]
        public async void Controller_IsAssignedToMe_Returns_ErrorResult_OnFailure()
        {
            Assignment.Setup(x => x.IsAssigned(It.IsAny<string>(), It.IsAny<string>()))
                .ThrowsAsync(new InvalidOperationException("Unknown exception"));

            var result = (ErrorResult) await Api.IsAssignedToMe("application", "000001");

            Assert.NotNull(result);
            Assert.Equal(400, result.StatusCode);
        }

        [Fact]
        public async void Controller_GetAllAssignments_Returns_Result_OnSuccess()
        {
            Assignment.Setup(x => x.Get(It.IsAny<string>(), It.IsAny<string>()))
                .ReturnsAsync(new List<Assignment>());

            var result = (HttpOkObjectResult) await Api.GetAllAssignments("application", "000001");

            Assert.NotNull(result);
            Assert.Equal(200, result.StatusCode);
            Assert.IsType<List<Assignment>>(result.Value);
        }

        [Fact]
        public async void Controller_GetAllAssignments_Returns_ErrorResult_OnFailure()
        {
            Assignment.Setup(x => x.Get(It.IsAny<string>(), It.IsAny<string>()))
                .ThrowsAsync(new InvalidOperationException("Unknown exception"));

            var result = (ErrorResult) await Api.GetAllAssignments("application", "000001");

            Assert.NotNull(result);
            Assert.Equal(400, result.StatusCode);
        }

        [Fact]
        public async void Controller_GetAllByUser_Returns_Result_OnSuccess()
        {
            Assignment.Setup(x => x.GetByUser(It.IsAny<string>(), It.IsAny<string>()))
                .ReturnsAsync(new List<Assignment>());

            var result = (HttpOkObjectResult) await Api.GetAllByUser("application", "jondoe");

            Assert.NotNull(result);
            Assert.Equal(200, result.StatusCode);
            Assert.IsType<List<Assignment>>(result.Value);
        }

        [Fact]
        public async void Controller_GetAllByUser_Returns_ErrorResult_OnFailure()
        {
            Assignment.Setup(x => x.GetByUser(It.IsAny<string>(), It.IsAny<string>()))
                .ThrowsAsync(new InvalidOperationException("Unknown exception"));

            var result = (ErrorResult)await Api.GetAllByUser("application", "jondoe");

            Assert.NotNull(result);
            Assert.Equal(400, result.StatusCode);
        }


       

        [Fact]
        public async void Controller_Assign_Returns_Result_OnSuccess()
        {
            Assignment.Setup(x => x.Assign(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<AssignmentRequest>()))
                .ReturnsAsync(new Assignment());

            var result = (HttpOkObjectResult) await Api.Assign("application", "000001", new AssignmentRequest());

            Assert.NotNull(result);
            Assert.Equal(200, result.StatusCode);
            Assert.IsType<Assignment>(result.Value);
        }
                
        [Fact]
        public async void Controller_Assign_Returns_ErrorResult_OnFailure()
        {
            Assignment.Setup(x => x.Assign(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<AssignmentRequest>()))
                .ThrowsAsync(new InvalidOperationException("Unknown exception"));

            var result = (ErrorResult) await Api.Assign("application", "000001", new AssignmentRequest());

            Assert.NotNull(result);
            Assert.Equal(400, result.StatusCode);
        }

        [Fact]
        public async void Controller_MultiAssign_Returns_Result_OnSuccess()
        {
            Assignment.Setup(x => x.MultiAssign(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<List<AssignmentRequest>>()))
                .ReturnsAsync(new List<Assignment>());

            var result = (HttpOkObjectResult)await Api.MultiAssign("application", "000001", new List<AssignmentRequest>());

            Assert.NotNull(result);
            Assert.Equal(200, result.StatusCode);
            Assert.IsType<List<Assignment>>(result.Value);
        }

        [Fact]
        public async void Controller_MultiAssign_Returns_ErrorResult_OnFailure()
        {
            Assignment.Setup(x => x.MultiAssign(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<List<AssignmentRequest>>()))                 
                .ThrowsAsync(new InvalidOperationException("Unknown exception"));

            var result = (ErrorResult)await Api.MultiAssign("application", "000001", new List<AssignmentRequest>());

            Assert.NotNull(result);
            Assert.Equal(400, result.StatusCode);
        }

        [Fact]
        public async void Controller_Unassign_Returns_Result_OnSuccess()
        {
            var result = (NoContentResult) await Api.Unassign("application", "000001", "VP of Sales");

            Assert.NotNull(result);
            Assert.Equal(204, result.StatusCode);
        }

        [Fact]
        public async void Controller_Unassign_Returns_ErrorResult_OnFailure()
        {
            Assignment.Setup(x => x.Unassign(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()))
                .Throws(new InvalidOperationException("Unknown exception"));

            var result = (ErrorResult) await Api.Unassign("application", "000001", "VP of Sales");

            Assert.NotNull(result);
            Assert.Equal(400, result.StatusCode);
        }

        public async void Controller_Release_Returns_Result_OnSuccess()
        {
            var result = (NoContentResult) await Api.Release("application", "000001", "VP of Sales");

            Assert.NotNull(result);
            Assert.Equal(204, result.StatusCode);
        }

        [Fact]
        public async void Controller_Release_Returns_ErrorResult_OnFailure()
        {
            Assignment.Setup(x => x.Release(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()))
                .Throws(new InvalidOperationException("Unknown exception"));

            var result = (ErrorResult) await Api.Release("application", "000001", "VP of Sales");

            Assert.NotNull(result);
            Assert.Equal(400, result.StatusCode);
        }

        [Fact]
        public async void Send_Invite_ReturnsSuccessWithInviteResult()
        {          
            InviteService.Setup(s => s.SendInvite(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<InviteRequest>(), false))
                 .ReturnsAsync(new Invite());
            var result = (HttpOkObjectResult)await Api.SendInvite("application", "000001", new InviteRequest());

            Assert.NotNull(result);
            Assert.Equal(result.StatusCode, 200);
        }

        [Fact]
        public async void Send_Invite_WithEmptyInviteEmail_ReturnsErrorResult()
        {
            InviteService.Setup(s => s.SendInvite(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<InviteRequest>(), true))
                 .ThrowsAsync(new InvalidOperationException("Unknown exception"));

          
            var obj = new InviteRequest()
            {
                InvitedBy = "suresh"
            };
            var result = (ErrorResult)await Api.SendInvite("application", "000001", obj);

            Assert.NotNull(result);
            Assert.Equal(result.StatusCode, 400);
        }


        [Fact]
        public async void Controller_Verify_Returns_ErrorResult_OnFailure()
        {
            InviteService.Setup(x => x.VerifyToken(It.IsAny<string>(), It.IsAny<InviteVerifyRequest>()))
                .ThrowsAsync(new InvalidOperationException("Unknown exception"));

            var objRequest = new InviteVerifyRequest()
            {
                InviteEmail = "suresh.b@sigmainfo.net",
                InviteRefId = "1234456"
            };


            var result = (ErrorResult)await Api.verify("application", objRequest);

            Assert.NotNull(result);
            Assert.Equal(400, result.StatusCode);
        }

    }
}
