﻿using Docitt.AssignmentEngine.Mocks;
using Docitt.AssignmentEngine.Services;
using LendFoundry.Email;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using Moq;
using System;
using Xunit;

namespace Docitt.AssignmentEngine.Tests
{
    public class InviteServiceTests
    {
        private FakeAssignmentRepository AssignmentRepository { get; }
        private FakeInviteRepository inviteRepository { get; }
        private Mock<IAssignmentService> asignmentService { get; }

        private Mock<IEmailService> emailService { get; }
       
        private Mock<ITenantTime> TenantTime { get; }
        private Mock<IEventHubClient> EventHub { get; }
        private Mock<IValidatorService> Validator { get; }
        private Mock<IEnsureHelper> EnsureHelper { get; }
        private IInviteService Service { get; }
        private Mock<ILogger> Logger { get; }

        public InviteServiceTests()
        {
            AssignmentRepository = new FakeAssignmentRepository();
            inviteRepository = new FakeInviteRepository();
            
            var configuration = GetConfiguration();
            TenantTime = new Mock<ITenantTime>();
            EventHub = new Mock<IEventHubClient>();
            Validator = new Mock<IValidatorService>();
            asignmentService = new Mock<IAssignmentService>();
            emailService = new Mock<IEmailService>();
            EnsureHelper = new Mock<IEnsureHelper>();
            Logger = new Mock<ILogger>();

            Service = new InviteService(
                asignmentService.Object, 
                inviteRepository,
                TenantTime.Object, 
                EventHub.Object,
                Validator.Object, 
                emailService.Object, 
                configuration,
                null,
                null,
                EnsureHelper.Object,
                Logger.Object);
        }


        private static Docitt.AssignmentEngine.AssignmentServiceConfiguration GetConfiguration()
        {
            const string configurationJson = " { " +
                                    "\"invatationmessagetemplate\": {" +
                                    "\"name\": \"InvitationMessageTemplate\"," +
                                    "\"version\": \"1.0\" " +
                                    "}," +
                                    "\"borrowerlendingpage\": \"http://docitt.dev.lendfoundry.com:9005/#/get-started/refid:{0}\"," +
                                    "\"affinitypartnerlendingpage\": \"http://docitt.dev.lendfoundry.com:9006/#/sign-up/refid:{0}\"," +
                                    "\"borrowerroles\": [" +
                                    "\"borrower\"," +
                                    "\"co-borrower\"" +
                                    "]," +
                                    "\"tokentimeout\":\"2880\"" +
                                   " }";
            var configuration = Newtonsoft.Json.JsonConvert.DeserializeObject<Docitt.AssignmentEngine.AssignmentServiceConfiguration>(configurationJson);

            return configuration;
        }

        [Fact]
        public void Service_Init_With_NullArguments_Throws_ArgumentException()
        {
            Assert.Throws<ArgumentException>(() => new AssignmentService(null, null, null, null));
        }

        [Fact]
        public void Service_Init_With_Valid_Arguments()
        {
            var configuration = GetConfiguration();
            var Service = new InviteService(
                asignmentService.Object, 
                inviteRepository,
                TenantTime.Object, 
                EventHub.Object,
                Validator.Object, 
                emailService.Object, 
                configuration,
                null,
                null,
                EnsureHelper.Object,
                Logger.Object);

            Assert.NotNull(Service);
        }

        [Fact]
        public async void SendInvite_WithNullInviteRequest_Throws_InvalidArgumentException()
        {
            //string[] roles = { "Borrower" };
            Validator.Setup(x => x.EnsureEntityId(It.IsAny<string>()));

            Validator.Setup(x => x.EnsureEntityType(It.IsAny<string>()))
                .Returns("application");

            InviteRequest newInvite = null;
            await Assert.ThrowsAsync<ArgumentException>(async () => await Service.SendInvite("application", "000001", newInvite, false));
        }

        [Fact]
        public async void SendInvite_WithoutInviteEmail_Throws_InvalidArgumentException()
        {
            //string[] roles = { "Borrower" };
            Validator.Setup(x => x.EnsureEntityId(It.IsAny<string>()));

            Validator.Setup(x => x.EnsureEntityType(It.IsAny<string>()))
                .Returns("application");

            var newInvite = new InviteRequest
            {
                InvitedBy = "Suresh.b@sigmainfo.net",
                InviteEmail = "",
                InviteFirstName = "suresh",
                InviteLastName = "lname",
                InviteMobileNumber = "234567891",
                Role = "buying agent"
            };

            await Assert.ThrowsAsync<ArgumentException>(async () => await Service.SendInvite("application", "000001", newInvite, false));

        }
        [Fact]
        public async void SendInvite_WithSuccess()
        {
            //string[] roles = { "Borrower" };
            Validator.Setup(x => x.EnsureEntityId(It.IsAny<string>()));

            Validator.Setup(x => x.EnsureEntityType(It.IsAny<string>()))
                .Returns("application");

            var newInvite = new InviteRequest
            {
                InvitedBy = "Suresh.b@sigmainfo.net",
                InviteEmail = "suresh.bari@sigmainfo.net",
                InviteFirstName = "suresh",
                InviteLastName = "lastname",
                InviteMobileNumber = "234567891",
                Role = "buying agent"
            };

            var addeduser = await Service.SendInvite("application", "000001", newInvite, false);
            Assert.NotNull(addeduser);
        }

        [Fact]
        public async void GetInvite_InvalidRefId_Failure()
        {
            //string[] roles = { "Borrower" };
            Validator.Setup(x => x.EnsureEntityId(It.IsAny<string>()));

            Validator.Setup(x => x.EnsureEntityType(It.IsAny<string>()))
                .Returns("application");

            var newInvite = new Invite
            {
                Id = "58c15b781272af20cc0b71161",
                InvitedBy = "Suresh.b@sigmainfo.net",
                InviteEmail = "",
                InviteFirstName = "suresh",
                InviteLastName = "lastname",
                InviteMobileNumber = "234567891",
                Role = "buying agent"
            };
            inviteRepository.Add(newInvite);
            //var data = await Service.Get("application", "58c15b781272af20cc0b71162");
            //Assert.Null(data);
             await Assert.ThrowsAsync<ArgumentException>(async () => await Service.Get("58c15b781272af20cc0b71161"));

        }

        [Fact]
        public async void GetInvite_ValidRefId_SuccessWithData()
        {
            //string[] roles = { "Borrower" };
            Validator.Setup(x => x.EnsureEntityId(It.IsAny<string>()));

            Validator.Setup(x => x.EnsureEntityType(It.IsAny<string>()))
                .Returns("application");

            var newInvite = new Invite
            {
                EntityId = "000001",
                EntityType = "application",
                Id = "58c15b781272af20cc0b71161",
                InvitedBy = "Suresh.b@sigmainfo.net",
                InviteEmail = "",
                InviteFirstName = "suresh",
                InviteLastName = "lastname",
                InviteMobileNumber = "234567891",
                Role = "buying agent",
                InvitationToken= "58c15b781272af20cc0b71161"
            };
            inviteRepository.AddWithId(newInvite);
            var data = await Service.Get("58c15b781272af20cc0b71161");
            Assert.NotNull(data);
            Assert.Equal(data.Id, newInvite.Id);
            // await Assert.ThrowsAsync<FormatException>(async () => await Service.Get("application", "58c15b781272af20cc0b71161"));
        }

        [Fact]
        public async void UpdateInvite_WithNullRequest_Thorw_ArgumentException()
        {
            Validator.Setup(x => x.EnsureEntityId(It.IsAny<string>()));

            Validator.Setup(x => x.EnsureEntityType(It.IsAny<string>()))
                .Returns("application");

            InviteUpdateRequest newRequest = null;

            await Assert.ThrowsAsync<ArgumentException>(async () => await Service.UpdateInvite("application", newRequest));

        }

        [Fact]
        public async void UpdateInvite_WithEmptyUserName_Throw_ArgumentException()
        {
            Validator.Setup(x => x.EnsureEntityId(It.IsAny<string>()));

            Validator.Setup(x => x.EnsureEntityType(It.IsAny<string>()))
                .Returns("application");

            InviteUpdateRequest newRequest = new InviteUpdateRequest()
            {
                InviteRefId = "58c15b781272af20cc0b71161",
                UserId = "",
                UserName = ""
            };

            await Assert.ThrowsAsync<ArgumentException>(async () => await Service.UpdateInvite("application", newRequest));

        }

        [Fact]
        public async void UpdateInvite_WithInvalidRefId_Throw_ArgumentException()
        {
            Validator.Setup(x => x.EnsureEntityId(It.IsAny<string>()));

            Validator.Setup(x => x.EnsureEntityType(It.IsAny<string>()))
                .Returns("application");

            var newInvite = new Invite
            {
                EntityId = "000001",
                EntityType = "application",
                Id = "58c15b781272af20cc0b71161",
                InvitedBy = "Suresh.b@sigmainfo.net",
                InviteEmail = "",
                InviteFirstName = "suresh",
                InviteLastName = "lastname",
                InviteMobileNumber = "234567891",
                Role = "buying agent"
            };

            inviteRepository.Add(newInvite);

            InviteUpdateRequest newRequest = new InviteUpdateRequest()
            {
                InviteRefId = "58c15b781272af20cc0b71162",
                UserId = "58c15b781272af20cc03421",
                UserName = "suresh"
            };

            await Assert.ThrowsAsync<ArgumentException>(async () => await Service.UpdateInvite("application", newRequest));

        }

        [Fact]
        public async void UpdateInvite_WithValidRefId_SuceessWithData()
        {
            Validator.Setup(x => x.EnsureEntityId(It.IsAny<string>()));

            Validator.Setup(x => x.EnsureEntityType(It.IsAny<string>()))
                .Returns("application");

            var newInvite = new Invite
            {
                EntityId = "000001",
                EntityType = "application",
                Id = "58c15b781272af20cc0b71161",
                InvitedBy = "Suresh.b@sigmainfo.net",
                InviteEmail = "suresh@suresh.com",
                InviteFirstName = "suresh",
                InviteLastName = "lastname",
                InviteMobileNumber = "234567891",
                Role = "buying agent",
                InvitationToken= "58c15b781272af20cc0b71161"
            };

            inviteRepository.AddWithId(newInvite);

            InviteUpdateRequest newRequest = new InviteUpdateRequest()
            {
                InviteRefId = "58c15b781272af20cc0b71161",
                UserId = "58c15b781272af20cc03421",
                UserName = "suresh@suresh.com"
            };
            var data = await Service.UpdateInvite("application", newRequest);
            //await Assert.ThrowsAsync<ArgumentException>(async () => await Service.UpdateInvite("application", newRequest));
            Assert.NotNull(data);
            Assert.Equal(data.UserId, newRequest.UserId);

        }

        [Fact]
        public async void UpdateInvite_WithAlreadyCompletedInvite_Throw_ArgumentExceptipon()
        {
            Validator.Setup(x => x.EnsureEntityId(It.IsAny<string>()));

            Validator.Setup(x => x.EnsureEntityType(It.IsAny<string>()))
                .Returns("application");

            var newInvite = new Invite
            {
                EntityId = "000001",
                EntityType = "application",
                Id = "58c15b781272af20cc0b71161",
                InvitedBy = "Suresh.b@sigmainfo.net",
                InviteEmail = "",
                InviteFirstName = "suresh",
                InviteLastName = "lastname",
                InviteMobileNumber = "234567891",
                Role = "buying agent",
                UserId = "58c15b781272af20cc03421",
                UserName = "suresh"
            };

            inviteRepository.AddWithId(newInvite);

            InviteUpdateRequest newRequest = new InviteUpdateRequest()
            {
                InviteRefId = "58c15b781272af20cc0b71161",
                UserId = "58c15b781272af20cc03421",
                UserName = "suresh"
            };

            await Assert.ThrowsAsync<ArgumentException>(async () => await Service.UpdateInvite("application", newRequest));

        }

        [Fact]
        public async void VerifyToken_WithInvalidRefID_Failure()
        {
            var configuration = GetConfiguration();
            //string[] roles = { "Borrower" };
            Validator.Setup(x => x.EnsureEntityId(It.IsAny<string>()));

            Validator.Setup(x => x.EnsureEntityType(It.IsAny<string>()))
                .Returns("application");

            var newInvite = new Invite
            {
                EntityId = "000001",
                EntityType = "application",
                Id = "58c15b781272af20cc0b71161",
                InvitedBy = "Suresh.b@sigmainfo.net",
                InviteEmail = "",
                InviteFirstName = "suresh",
                InviteLastName = "lastname",
                InviteMobileNumber = "234567891",
                Role = "buying agent",
                InvitationToken = "58c15b781272af20c",
                InvitationTokenExpiry = DateTime.UtcNow.AddMinutes(configuration.TokenTimeout)
            };
            inviteRepository.Add(newInvite);

            var objRequest = new InviteVerifyRequest()
            {
                InviteEmail = "suresh.b@sigmainfo.net",
                InviteRefId = "1234456"
            };


            await Assert.ThrowsAsync<ArgumentException>(async () => await Service.VerifyToken("application", objRequest));


        }

        [Fact]
        public async void VerifyToken_WithvalidExpiryTokenRefID_Failure()
        {
            var configuration = GetConfiguration();
            //string[] roles = { "Borrower" };
            Validator.Setup(x => x.EnsureEntityId(It.IsAny<string>()));

            Validator.Setup(x => x.EnsureEntityType(It.IsAny<string>()))
                .Returns("application");

            var newInvite = new Invite
            {
                EntityId="000001",
                EntityType="application",
                Id = "58c15b781272af20cc0b71161",
                InvitedBy = "Suresh.b@sigmainfo.net",
                InviteEmail = "",
                InviteFirstName = "suresh",
                InviteLastName = "lastname",
                InviteMobileNumber = "234567891",
                Role = "buying agent",
                InvitationToken= "58c15b781272af20cc0b71162",
                InvitationTokenExpiry = DateTime.UtcNow.AddMinutes(-configuration.TokenTimeout- 30)
        };
            inviteRepository.Add(newInvite);
            var objRequest = new InviteVerifyRequest()
            {
                InviteEmail = "suresh.b@sigmainfo.net",
                InviteRefId = "58c15b781272af20cc0b71162"
            };
            await Assert.ThrowsAsync<ArgumentException>(async () => await Service.VerifyToken("application", objRequest));


        }

        [Fact]
        public async void VerifyToken_WithvalidRefID_SuccessWithData()
        {
            var configuration = GetConfiguration();
            //string[] roles = { "Borrower" };
            Validator.Setup(x => x.EnsureEntityId(It.IsAny<string>()));

            Validator.Setup(x => x.EnsureEntityType(It.IsAny<string>()))
                .Returns("application");

            var newInvite = new Invite
            {
                EntityId = "000001",
                EntityType = "application",
                Id = "58c15b781272af20cc0b71161",
                InvitedBy = "Suresh.b@sigmainfo.net",
                InviteEmail = "suresh.b@sigmainfo.net",
                InviteFirstName = "suresh",
                InviteLastName = "lastname",
                InviteMobileNumber = "234567891",
                Role = "buying agent",
                InvitationToken = "58c15b781272af20cc0b71162",
                InvitationTokenExpiry = DateTime.UtcNow.AddMinutes(configuration.TokenTimeout - 30)
            };
            inviteRepository.Add(newInvite);

            var objRequest = new InviteVerifyRequest()
            {
                InviteEmail = "suresh.b@sigmainfo.net",
                InviteRefId = "58c15b781272af20cc0b71162"
            };

            var data = await Service.VerifyToken("application", objRequest);
            //await Assert.ThrowsAsync<ArgumentException>(async () => await Service.UpdateInvite("application", newRequest));
            Assert.NotNull(data);
            Assert.Equal(data.InvitationToken, newInvite.InvitationToken);


        }
    }
}
