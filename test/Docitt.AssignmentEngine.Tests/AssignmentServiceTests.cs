﻿using Docitt.AssignmentEngine.Mocks;
using Docitt.AssignmentEngine.Services;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Services;
using Moq;
using System;
using Xunit;

namespace Docitt.AssignmentEngine.Tests
{
    public class AssignmentServiceTests
    {
        private FakeAssignmentRepository AssignmentRepository { get; }
      
        private Mock<ITenantTime> TenantTime { get; }
        private Mock<IEventHubClient> EventHub { get; }
        private Mock<IValidatorService> Validator { get; }
      
        private IAssignmentService Service { get; }

        public AssignmentServiceTests()
        {
            AssignmentRepository = new FakeAssignmentRepository();

            TenantTime = new Mock<ITenantTime>();
            EventHub = new Mock<IEventHubClient>();
            Validator = new Mock<IValidatorService>();
            
            Service = new AssignmentService(AssignmentRepository,
                TenantTime.Object
                , EventHub.Object,
                Validator.Object);
        }

        [Fact]
        public void Service_Init_With_NullArguments_Throws_ArgumentException()
        {
            Assert.Throws<ArgumentException>(() => new AssignmentService(null,  null, null, null));
        }

        [Fact]
        public void Service_Init_With_Valid_Arguments()
        {
            var service = new AssignmentService(AssignmentRepository,
                TenantTime.Object
                , EventHub.Object,
                Validator.Object);

            Assert.NotNull(service);
        }

        [Fact]
        public async void Service_IsAssigned_With_Invalid_EntityId_Throws_ArgumentException()
        {
            Validator.Setup(x => x.EnsureEntityId(It.IsAny<string>()))
                .Throws(new ArgumentException());

            await  Assert.ThrowsAsync<ArgumentException>(async () => await Service.IsAssigned("application", ""));

            Validator.Verify(x => x.EnsureEntityId(It.IsAny<string>()), Times.AtLeastOnce);
        }

        [Fact]
        public async void Service_IsAssigned_With_Invalid_EntityType_Throws_InvalidArgumentException()
        {
            Validator.Setup(x => x.EnsureEntityId(It.IsAny<string>()));

            Validator.Setup(x => x.EnsureEntityType(It.IsAny<string>()))
                .Throws(new InvalidArgumentException(""));

            await Assert.ThrowsAsync<InvalidArgumentException>(async () => await Service.IsAssigned("", "000001"));

            Validator.Verify(x => x.EnsureEntityId(It.IsAny<string>()), Times.AtLeastOnce);
            Validator.Verify(x => x.EnsureEntityType(It.IsAny<string>()), Times.AtLeastOnce);
        }

        [Fact]
        public async void Service_IsAssigned_With_Valid_Arguments_Returns_Username_If_It_Exists()
        {
            Validator.Setup(x => x.EnsureEntityId(It.IsAny<string>()));

            Validator.Setup(x => x.EnsureEntityType(It.IsAny<string>()))
                .Returns("application");

            Validator.Setup(x => x.EnsureCurrentUser())
                .Returns("suresh");

            await Service.IsAssigned("application", "000001");

            Validator.Verify(x => x.EnsureEntityId(It.IsAny<string>()), Times.AtLeastOnce);
            Validator.Verify(x => x.EnsureEntityType(It.IsAny<string>()), Times.AtLeastOnce);
            Validator.Verify(x => x.EnsureCurrentUser(), Times.AtLeastOnce);
        }

        [Fact]
        public async void Service_IsAssigned_With_Valid_Arguments_Returns_AssignedResponse_With_True_If_It_Exists_For_Me()
        {
            Validator.Setup(x => x.EnsureEntityId(It.IsAny<string>()));

            Validator.Setup(x => x.EnsureEntityType(It.IsAny<string>()))
                .Returns("application");

            Validator.Setup(x => x.EnsureCurrentUser())
                .Returns("suresh");

            AssignmentRepository.Add(new Assignment { EntityType = "application", EntityId = "000001", Role = "VP of Sales", Assignee = "suresh", IsActive = true, IsDisable = false });



            var result = await Service.IsAssigned("application", "000001");

            Validator.Verify(x => x.EnsureEntityId(It.IsAny<string>()), Times.AtLeastOnce);
            Validator.Verify(x => x.EnsureEntityType(It.IsAny<string>()), Times.AtLeastOnce);
            Validator.Verify(x => x.EnsureCurrentUser(), Times.AtLeastOnce);

            Assert.NotNull(result);
            Assert.True(result.IsAssigned);
        }

        [Fact]
        public async void Service_IsAssigned_With_Valid_Arguments_Returns_AssignedResponse_With_False_If_It_Exists_For_Me()
        {
            Validator.Setup(x => x.EnsureEntityId(It.IsAny<string>()));

            Validator.Setup(x => x.EnsureEntityType(It.IsAny<string>()))
                .Returns("application");

            Validator.Setup(x => x.EnsureCurrentUser())
                .Returns("suresh");

            var result = await Service.IsAssigned("application", "000001");

            Validator.Verify(x => x.EnsureEntityId(It.IsAny<string>()), Times.AtLeastOnce);
            Validator.Verify(x => x.EnsureEntityType(It.IsAny<string>()), Times.AtLeastOnce);
            Validator.Verify(x => x.EnsureCurrentUser(), Times.AtLeastOnce);

            Assert.NotNull(result);
            Assert.False(result.IsAssigned);
        }

        [Fact]
        public async void Service_Get_With_Invalid_EntityId_Throws_ArgumentException()
        {
            Validator.Setup(x => x.EnsureEntityId(It.IsAny<string>()))
                .Throws(new ArgumentException());

            await Assert.ThrowsAsync<ArgumentException>(async () => await Service.Get("application", ""));

            Validator.Verify(x => x.EnsureEntityId(It.IsAny<string>()), Times.AtLeastOnce);
        }

        [Fact]
        public async void Service_Get_With_Invalid_EntityType_Throws_InvalidArgumentException()
        {
            Validator.Setup(x => x.EnsureEntityId(It.IsAny<string>()));

            Validator.Setup(x => x.EnsureEntityType(It.IsAny<string>()))
                .Throws(new InvalidArgumentException(""));

            await Assert.ThrowsAsync<InvalidArgumentException>(async () => await Service.Get("", "000001"));

            Validator.Verify(x => x.EnsureEntityId(It.IsAny<string>()), Times.AtLeastOnce);
            Validator.Verify(x => x.EnsureEntityType(It.IsAny<string>()), Times.AtLeastOnce);
        }

        [Fact]
        public async void Service_Get_With_Valid_Arguments_Returns_Assignments()
        {
            Validator.Setup(x => x.EnsureEntityId(It.IsAny<string>()));

            Validator.Setup(x => x.EnsureEntityType(It.IsAny<string>()))
                .Returns("application");

            var result = await Service.Get("application", "000001");

            Validator.Verify(x => x.EnsureEntityId(It.IsAny<string>()), Times.AtLeastOnce);
            Validator.Verify(x => x.EnsureEntityType(It.IsAny<string>()), Times.AtLeastOnce);

            Assert.NotNull(result);
        }

        [Fact]
        public async void Service_GetByUser_With_Invalid_Username_Throws_ArgumentException()
        {
            await Assert.ThrowsAsync<ArgumentException>(async () => await Service.GetByUser("application", ""));
        }

        [Fact]
        public async void Service_GetByUser_With_Invalid_EntityType_Throws_InvalidArgumentException()
        {
            Validator.Setup(x => x.EnsureEntityType(It.IsAny<string>()))
                .Throws(new InvalidArgumentException(""));

            await Assert.ThrowsAsync<InvalidArgumentException>(async () => await Service.GetByUser("application", "suresh"));

            Validator.Verify(x => x.EnsureEntityType(It.IsAny<string>()), Times.AtLeastOnce);
        }

        [Fact]
        public async void Service_GetByUser_With_Valid_Arguments_Returns_Assignments()
        {
            Validator.Setup(x => x.EnsureEntityType(It.IsAny<string>()))
                .Returns("application");

            var result = await Service.GetByUser("application", "suresh");

            Validator.Verify(x => x.EnsureEntityType(It.IsAny<string>()), Times.AtLeastOnce);

            Assert.NotNull(result);
        }

       

        [Fact]
        public async void Service_Assign_With_Valid_Arguments_Returns_Throws_Exception_If_It_Is_Already_Assigned()
        {
            Validator.Setup(x => x.EnsureCurrentUser())
                .Returns("suresh");

            Validator.Setup(x => x.EnsureEntityId(It.IsAny<string>()));

            Validator.Setup(x => x.EnsureEntityType(It.IsAny<string>()))
               .Returns("application");

            //Validator.Setup(x => x.EnsureUserRole(It.IsAny<string>(), It.IsAny<string>()))
            //   .ReturnsAsync("VP of Sales");

            AssignmentRepository.Add(new Assignment {EntityType="application",EntityId="000001", Role = "VP of Sales", Assignee = "suresh",IsActive=true,IsDisable=false });
                
                

               await
                Assert.ThrowsAsync<InvalidOperationException>(
                    async () =>
                        await
                            Service.Assign("application", "000001",
                                new AssignmentRequest {Role = "VP of Sales", User = "suresh"}));

            Validator.Verify(x => x.EnsureEntityId(It.IsAny<string>()), Times.AtLeastOnce);
            Validator.Verify(x => x.EnsureEntityType(It.IsAny<string>()), Times.AtLeastOnce);
            //Validator.Verify(x => x.EnsureUserRole(It.IsAny<string>(), It.IsAny<string>()), Times.AtLeastOnce);
        }

        [Fact]
        public async void Service_Assign_Valid_Arguments_Returns_Assigment_If_It_Is_Not_Assigned()
        {
            //Validator.Setup(x => x.EnsureCurrentUser())
            //    .Returns("suresh");

            Validator.Setup(x => x.EnsureEntityId(It.IsAny<string>()));

            Validator.Setup(x => x.EnsureEntityType(It.IsAny<string>()))
               .Returns("application");

         
            EventHub.Setup(x => x.Publish(It.IsAny<string>(), It.IsAny<object>()))
                .ReturnsAsync(true);

            var result =
                await
                    Service.Assign("application", "000001",
                        new AssignmentRequest {Role = "VP of Sales", User = "suresh"});


            //Validator.Verify(x => x.EnsureCurrentUser(), Times.AtLeastOnce);
            Validator.Verify(x => x.EnsureEntityId(It.IsAny<string>()), Times.AtLeastOnce);
            Validator.Verify(x => x.EnsureEntityType(It.IsAny<string>()), Times.AtLeastOnce);
           
            EventHub.Verify(x => x.Publish(It.IsAny<string>(), It.IsAny<object>()), Times.AtLeastOnce);

            Assert.NotNull(result);
        }

        [Fact]
        public async void Service_Assign_With_Invalid_Username_Throws_ArgumentException()
        {
            await Assert.ThrowsAsync<ArgumentException>(async () => await
                Service.Assign("application", "000001",
                    new AssignmentRequest {Role = "VP of Sales", User = ""}));
        }

        [Fact]
        public async void Service_Assign_With_Invalid_EntityId_Throws_ArgumentException()
        {
            Validator.Setup(x => x.EnsureEntityId(It.IsAny<string>()))
                .Throws(new ArgumentException());

            await
                Assert.ThrowsAsync<ArgumentException>(
                    async () =>
                        await
                            Service.Assign("application", "000001",
                                new AssignmentRequest {Role = "VP of Sales", User = "suresh"}));

            Validator.Verify(x => x.EnsureEntityId(It.IsAny<string>()), Times.AtLeastOnce);
        }

        [Fact]
        public async void Service_Assign_With_Invalid_EntityType_Throws_InvalidArgumentException()
        {
            Validator.Setup(x => x.EnsureEntityId(It.IsAny<string>()));

            Validator.Setup(x => x.EnsureEntityType(It.IsAny<string>()))
                .Throws(new InvalidArgumentException(""));

            await Assert.ThrowsAsync<InvalidArgumentException>(async () =>
                await
                    Service.Assign("application", "000001",
                        new AssignmentRequest {Role = "VP of Sales", User = "suresh"}));

            Validator.Verify(x => x.EnsureEntityId(It.IsAny<string>()), Times.AtLeastOnce);
            Validator.Verify(x => x.EnsureEntityType(It.IsAny<string>()), Times.AtLeastOnce);
        }

        [Fact]
        public async void Service_Unassign_With_Invalid_Role_Throws_ArgumentException()
        {
            await Assert.ThrowsAsync<ArgumentException>(async () => await
                Service.Unassign("application", "000001", ""));
        }

        [Fact]
        public async void Service_Unassign_With_Invalid_EntityId_Throws_ArgumentException()
        {
            Validator.Setup(x => x.EnsureEntityId(It.IsAny<string>()))
                .Throws(new ArgumentException());

            await
                Assert.ThrowsAsync<ArgumentException>(
                    async () =>
                        await
                            Service.Unassign("application", "000001", "VP of Sales"));

            Validator.Verify(x => x.EnsureEntityId(It.IsAny<string>()), Times.AtLeastOnce);
        }

        [Fact]
        public async void Service_Unassign_With_Invalid_EntityType_Throws_InvalidArgumentException()
        {
            Validator.Setup(x => x.EnsureEntityId(It.IsAny<string>()));

            Validator.Setup(x => x.EnsureEntityType(It.IsAny<string>()))
                .Throws(new InvalidArgumentException(""));

            await Assert.ThrowsAsync<InvalidArgumentException>(async () =>
                await
                    Service.Unassign("application", "000001", "VP of Sales"));

            Validator.Verify(x => x.EnsureEntityId(It.IsAny<string>()), Times.AtLeastOnce);
            Validator.Verify(x => x.EnsureEntityType(It.IsAny<string>()), Times.AtLeastOnce);
        }

        [Fact]
        public async void Service_Unassign_With_Non_Existing_Role_Throws_NotFoundException()
        {
            Validator.Setup(x => x.EnsureEntityId(It.IsAny<string>()));

            Validator.Setup(x => x.EnsureEntityType(It.IsAny<string>()))
                .Returns("application");

            await Assert.ThrowsAsync<NotFoundException>(async () =>
                await
                    Service.Unassign("application", "000001", "VP of Sales"));

            Validator.Verify(x => x.EnsureEntityId(It.IsAny<string>()), Times.AtLeastOnce);
            Validator.Verify(x => x.EnsureEntityType(It.IsAny<string>()), Times.AtLeastOnce);
        }

        [Fact]
        public async void Service_Unassign_With_Valid_Arguments_Removes_Assignment()
        {
            Validator.Setup(x => x.EnsureEntityId(It.IsAny<string>()));

            Validator.Setup(x => x.EnsureEntityType(It.IsAny<string>()))
                .Returns("application");

           

            EventHub.Setup(x => x.Publish(It.IsAny<string>(), It.IsAny<object>()))
                .ReturnsAsync(true);
            AssignmentRepository.Add(new Assignment { EntityType = "application", EntityId = "000001", Role = "VP of Sales", Assignee = "suresh", IsActive = true, IsDisable = false });



            await Service.Unassign("application", "000001", "suresh");

            Validator.Verify(x => x.EnsureEntityId(It.IsAny<string>()), Times.AtLeastOnce);
            Validator.Verify(x => x.EnsureEntityType(It.IsAny<string>()), Times.AtLeastOnce);
            //Validator.Verify(x => x.EnsureCurrentUser(), Times.AtLeastOnce);

            EventHub.Verify(x => x.Publish(It.IsAny<string>(), It.IsAny<object>()), Times.AtLeastOnce);
        }

        [Fact]
        public async void Service_Release_With_Valid_Arguments_Removes_Assignment()
        {
            //Validator.Setup(x => x.EnsureUserRole(It.IsAny<string>(), It.IsAny<string>()))
            //    .ReturnsAsync("VP of Sales");

            Validator.Setup(x => x.EnsureEntityId(It.IsAny<string>()));

            Validator.Setup(x => x.EnsureEntityType(It.IsAny<string>()))
                .Returns("application");

            Validator.Setup(x => x.EnsureCurrentUser())
                .Returns("suresh");

            EventHub.Setup(x => x.Publish(It.IsAny<string>(), It.IsAny<object>()))
                .ReturnsAsync(true);
            AssignmentRepository.Add(new Assignment { EntityType = "application", EntityId = "000001", Role = "VP of Sales", Assignee = "suresh", IsActive = true, IsDisable = false });


            await Service.Release("application", "000001", "suresh");

           // Validator.Verify(x => x.EnsureUserRole(It.IsAny<string>(), It.IsAny<string>()), Times.AtLeastOnce);
            Validator.Verify(x => x.EnsureEntityId(It.IsAny<string>()), Times.AtLeastOnce);
            Validator.Verify(x => x.EnsureEntityType(It.IsAny<string>()), Times.AtLeastOnce);
            Validator.Verify(x => x.EnsureCurrentUser(), Times.AtLeast(1));

            EventHub.Verify(x => x.Publish(It.IsAny<string>(), It.IsAny<object>()), Times.AtLeastOnce);
        }

        
        [Fact]
        public async void Service_Release_Allowed_An_Entity_To_SRM_By_SRM()
        {
            //Validator.Setup(x => x.EnsureUserRole(It.IsAny<string>(), It.IsAny<string>()))
             //   .ReturnsAsync("VP of Sales");

            Validator.Setup(x => x.EnsureEntityId(It.IsAny<string>()));

            Validator.Setup(x => x.EnsureEntityType(It.IsAny<string>()))
                .Returns("application");

            Validator.Setup(x => x.EnsureCurrentUser())
                .Returns("suresh");

            EventHub.Setup(x => x.Publish(It.IsAny<string>(), It.IsAny<object>()))
                .ReturnsAsync(true);
            AssignmentRepository.Add(new Assignment { EntityType = "application", EntityId = "000001", Role = "VP of Sales", Assignee = "suresh", IsActive = true, IsDisable = false });


            await Service.Release("application", "000001", "suresh");

            //Validator.Verify(x => x.EnsureUserRole(It.IsAny<string>(), It.IsAny<string>()), Times.AtLeastOnce);
            Validator.Verify(x => x.EnsureEntityId(It.IsAny<string>()), Times.AtLeastOnce);
            Validator.Verify(x => x.EnsureEntityType(It.IsAny<string>()), Times.AtLeastOnce);
            Validator.Verify(x => x.EnsureCurrentUser(), Times.AtLeast(1));

            EventHub.Verify(x => x.Publish(It.IsAny<string>(), It.IsAny<object>()), Times.AtLeastOnce);
        }

        [Fact]
        public async void Service_IsAssigned_An_Entity_To_VPOfSales_Returns_True()
        {
            Validator.Setup(x => x.EnsureEntityId(It.IsAny<string>()));

            Validator.Setup(x => x.EnsureEntityType(It.IsAny<string>()))
                .Returns("application");

            Validator.Setup(x => x.EnsureCurrentUser())
                .Returns("suresh");
            AssignmentRepository.Add(new Assignment { EntityType = "application", EntityId = "000001", Role = "VP of Sales", Assignee = "suresh", IsActive = true, IsDisable = false });


            var result = await Service.IsAssigned("application", "000001");

            Validator.Verify(x => x.EnsureEntityId(It.IsAny<string>()), Times.AtLeastOnce);
            Validator.Verify(x => x.EnsureEntityType(It.IsAny<string>()), Times.AtLeastOnce);
            Validator.Verify(x => x.EnsureCurrentUser(), Times.AtLeastOnce);

            Assert.NotNull(result);
            Assert.True(result.IsAssigned);
        }

        [Fact]
        public async void Service_IsAssigned_An_Entity_To_QAM_Returns_False()
        {
            Validator.Setup(x => x.EnsureEntityId(It.IsAny<string>()));

            Validator.Setup(x => x.EnsureEntityType(It.IsAny<string>()))
                .Returns("application");

            Validator.Setup(x => x.EnsureCurrentUser())
                .Returns("swaraj");

            var result = await Service.IsAssigned("application", "000001");

            Validator.Verify(x => x.EnsureEntityId(It.IsAny<string>()), Times.AtLeastOnce);
            Validator.Verify(x => x.EnsureEntityType(It.IsAny<string>()), Times.AtLeastOnce);
            Validator.Verify(x => x.EnsureCurrentUser(), Times.AtLeastOnce);

            Assert.NotNull(result);
            Assert.False(result.IsAssigned);
        }

        [Fact]
        public async void Service_GetAll_Assignments()
        {
            Validator.Setup(x => x.EnsureEntityId(It.IsAny<string>()));

            Validator.Setup(x => x.EnsureEntityType(It.IsAny<string>()))
                .Returns("application");

            var result = await Service.Get("application", "000001");

            Validator.Verify(x => x.EnsureEntityId(It.IsAny<string>()), Times.AtLeastOnce);
            Validator.Verify(x => x.EnsureEntityType(It.IsAny<string>()), Times.AtLeastOnce);

            Assert.NotNull(result);
        }

        [Fact]
        public async void Service_GetAll_Assignments_By_Username()
        {
            Validator.Setup(x => x.EnsureEntityType(It.IsAny<string>()))
                .Returns("application");

            var result = await Service.GetByUser("application", "suresh");

            Validator.Verify(x => x.EnsureEntityType(It.IsAny<string>()), Times.AtLeastOnce);

            Assert.NotNull(result);
        }


        
    }
}
